import java.text.SimpleDateFormat
import java.util.*

plugins {
    eclipse
    idea
    `java-library`
    `maven-publish`
    signing
    id("com.palantir.git-version") version "0.12.3"
    id("com.github.mrcjkb.module-finder") version "0.0.5"
}

description = "Polysun plugin with controllers for Polysun simulations."
group = "com.velasolaris.polysun"
val gitVersion: groovy.lang.Closure<String> by extra
version = gitVersion()
        .replace(".dirty", "")
        .replace("-", ".")
        .replaceAfter("SNAPSHOT", "")
val isReleaseVersion = !version.toString().endsWith("SNAPSHOT")

repositories {
    mavenCentral()
}

java {
    toolchain {
        languageVersion.set(JavaLanguageVersion.of(17))
        vendor.set(JvmVendorSpec.BELLSOFT)
    }
    withJavadocJar()
    withSourcesJar()
}

val pluginIfVersion = "1.1.0"
val jsonrpc2ClientVersion = "1.16.5"

dependencies {
    api("com.velasolaris.polysun:polysun-plugin-if:$pluginIfVersion")
    implementation("org.apache.commons:commons-lang3:3.12.0")
    implementation("com.diffplug.matsim:matconsolectl:4.6.0")

    // use a fork of the org.apache.xmlrpc:xmlrpc-client library.
    // The original library is unmaintained since 2010 and has vulnerability
    // so we use this fork which contains additional security fixes
    // https://github.com/evolvedbinary/apache-xmlrpc
    implementation("com.evolvedbinary.thirdparty.org.apache.xmlrpc:xmlrpc-client:5.0.0") {
        exclude(group="xml-apis", module="xml-apis")
        exclude(group="junit", module="junit")
        exclude(group="net.minidev", module="json-smart")

        // suppress the xmlrpc-common dependency because it redundantly contains most classes as the main xmlrpc-client
        // lib and the duplicate presence of these same classes would break our java module build
        exclude(group="com.evolvedbinary.thirdparty.org.apache.xmlrpc", module="xmlrpc-common")
    }
    implementation("org.apache.ws.commons.util:ws-commons-util:1.0.2") {
        exclude(group="xml-apis", module="xml-apis")
        exclude(group="junit", module="junit")
    }

    constraints {
        // force newer, more secure version of json-smart library than is used by the xmlrpc-client lib
        implementation("net.minidev:json-smart:2.5.1") {
            because ("earlier versions have a security vulnerability")
        }
    }
    implementation("com.thetransactioncompany:jsonrpc2-client:$jsonrpc2ClientVersion")

    testImplementation("org.junit.jupiter:junit-jupiter-engine:5.10.3")
}

tasks {
    javadoc {
        title = "Polysun Public Plugin"
        val standardJavadocDocletOptions = options as StandardJavadocDocletOptions
        standardJavadocDocletOptions.addBooleanOption("html5", true)
        standardJavadocDocletOptions.stylesheetFile = project.file("javadoc_stylesheet.css")
        standardJavadocDocletOptions.links?.add("https://docs.oracle.com/en/java/javase/17/docs/api")
        standardJavadocDocletOptions.links?.add("https://javadoc.io/doc/com.velasolaris.polysun/polysun-plugin-if/$pluginIfVersion")
        standardJavadocDocletOptions.links?.add("https://javadoc.io/doc/com.thetransactioncompany/jsonrpc2-client/$jsonrpc2ClientVersion")
        standardJavadocDocletOptions.addStringOption("Xdoclint:none", "-quiet")
    }
    jar {
        val javaVersion = System.getProperty("java.version")
        val javaVendor = System.getProperty("java.vendor")
        val javaVmVersion = System.getProperty("java.vm.version")
        val osName = System.getProperty("os.name")
        val osArchitecture = System.getProperty("os.arch")
        val osVersion = System.getProperty("os.version")
        manifest {
            attributes["Library"] = rootProject.name
            attributes["Version"] = archiveVersion
            attributes["Company"] = "Vela Solaris AG"
            attributes["Website"] = "www.velasolaris.com"
            attributes["Built-By"] = System.getProperty("user.name")
            attributes["Build-Timestamp"] = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ").format(Date())
            attributes["Created-by"] = "Gradle ${gradle.gradleVersion}"
            attributes["Build-OS"] = "$osName $osArchitecture $osVersion"
            attributes["Build-Jdk"] = "$javaVersion ($javaVendor $javaVmVersion)"
            attributes["Build-OS"] = "$osName $osArchitecture $osVersion"
        }
    }
    test {
        useJUnitPlatform()
    }
}

configurePublication(rootProject)

fun configurePublication(project: Project) {
    publishing {
        publications {
            create<MavenPublication>(project.name) {
                groupId = group.toString()
                artifactId = project.name
                version = version
                from(project.components["java"])
                versionMapping {
                    usage("java-api") {
                        fromResolutionOf("runtimeClasspath")
                    }
                    usage("java-runtime") {
                        fromResolutionResult()
                    }
                }
                pom {
                    name.set(project.name)
                    description.set(project.description)
                    url.set("https://bitbucket.org/velasolaris/polysun-public-plugin")
                    developers {
                        developer {
                            id.set("rkurmann")
                            name.set("Roland Kurmann")
                        }
                        developer {
                            id.set("mjakobi")
                            name.set("Marc Jakobi")
                        }
                    }
                    issueManagement {
                        system.set("Bitbucket")
                        url.set("https://bitbucket.org/velasolaris/polysun-public-plugin/issues")
                    }
                    scm {
                        url.set("https://bitbucket.org/velasolaris/polysun-public-plugin")
                        connection.set("scm:git:git://bitbucket.org/velasolaris/polysun-public-plugin.git")
                        developerConnection.set("scm:git:ssh://git@bitbucket.org:velasolaris/polysun-public-plugin.git")
                    }
                    licenses {
                        license {
                            name.set("MIT license")
                            url.set("https://bitbucket.org/velasolaris/polysun-public-plugin/src/master/LICENSE.txt")
                            distribution.set("repo")
                        }
                    }
                }
            }
            repositories {
                maven {
                    val releasesRepoUrl = uri("https://oss.sonatype.org/service/local/staging/deploy/maven2/")
                    val snapshotsRepoUrl = uri("https://oss.sonatype.org/content/repositories/snapshots/")
                    url = if (isReleaseVersion) releasesRepoUrl else snapshotsRepoUrl
                    credentials {
                        username = project.properties["ossrhUser"]?.toString() ?: "Unknown user"
                        password = project.properties["ossrhPassword"]?.toString() ?: "Unknown password"
                    }
                }
            }
        }
    }
    signing {
        if (isReleaseVersion) {
            sign(publishing.publications[project.name])
        }
    }
}

