module com.velasolaris.polysun.open.plugin {
    requires com.velasolaris.polysun.plugin.api;
    requires org.apache.commons.lang3;
    requires java.desktop;
    requires java.logging;
    requires jsonrpc2.base;
    exports com.velasolaris.plugin.controller.flowrate;
    exports com.velasolaris.plugin.controller.matlab.matconsolectl;
    exports com.velasolaris.plugin.controller.rpc;
    exports com.velasolaris.plugin.controller.publicplugin;
}
