package com.velasolaris.plugin.painter;

import com.velasolaris.plugin.controller.spi.IPluginImagePainter;

import java.awt.*;
import java.awt.geom.AffineTransform;
import java.awt.geom.GeneralPath;
import java.awt.geom.Rectangle2D;

import static java.awt.Color.WHITE;

/**
 * Paints the Matlab plugin controller in the Polysun designer.
 */
public class MatlabPluginControllerPainter implements IPluginImagePainter {

    @Override
    public void paint(Graphics2D g) {
        Shape shape = null;

        float origAlpha = 1.0f;
        Composite origComposite = g.getComposite();
        if (origComposite instanceof AlphaComposite) {
            AlphaComposite origAlphaComposite = (AlphaComposite)origComposite;
            if (origAlphaComposite.getRule() == AlphaComposite.SRC_OVER) {
                origAlpha = origAlphaComposite.getAlpha();
            }
        }

        java.util.LinkedList<AffineTransform> transformations = new java.util.LinkedList<AffineTransform>();


        //

        // _0
        transformations.push(g.getTransform());
        g.transform(new AffineTransform(1, 0, 0, 1, 31.072315f, -38.340137f));

        // _0_0

        g.setTransform(transformations.pop()); // _0_0
        transformations.push(g.getTransform());
        g.transform(new AffineTransform(0.90677965f, 0, 0, 0.90677965f, 12.552985f, -38.65011f));

        // _0_1
        transformations.push(g.getTransform());
        g.transform(new AffineTransform(1, 0, 0, 1, 19.028038f, 1.4636421f));

        // _0_1_0
        transformations.push(g.getTransform());
        g.transform(new AffineTransform(0.39790422f, 0, 0, 0.39790422f, 0.83772373f, 1.7224818f));

        // _0_1_0_0
        transformations.push(g.getTransform());
        g.transform(new AffineTransform(1, 0, 0, 1, 30.69656f, -0.72493494f));

        // _0_1_0_0_0
        transformations.push(g.getTransform());
        g.transform(new AffineTransform(0, 0.971383f, -0.9713829f, 0, -13.19697f, 62.049095f));

        // _0_1_0_0_0_0

        g.setTransform(transformations.pop()); // _0_1_0_0_0_0
        transformations.push(g.getTransform());
        g.transform(new AffineTransform(1, 0, 0, 1, 4.5428276f, -0.255567f));

        // _0_1_0_0_0_1
        transformations.push(g.getTransform());
        g.transform(new AffineTransform(0.7497474f, 0, 0, 0.7509831f, -82.78321f, 65.3706f));

        // _0_1_0_0_0_1_0

        g.setTransform(transformations.pop()); // _0_1_0_0_0_1_0
        transformations.push(g.getTransform());
        g.transform(new AffineTransform(1, 0, 0, 1, -90.646484f, 70.39424f));

        // _0_1_0_0_0_1_1

        // _0_1_0_0_0_1_1_0
        transformations.push(g.getTransform());
        g.transform(new AffineTransform(1, 0, 0, 1, 90.646484f, -70.39424f));

        // _0_1_0_0_0_1_1_0_0
        transformations.push(g.getTransform());
        g.transform(new AffineTransform(1, 0, 0, 1, 0.01350987f, 0.02869167f));

        // _0_1_0_0_0_1_1_0_0_0

        // _0_1_0_0_0_1_1_0_0_0_0
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(-119.9707, 100.06445);
        ((GeneralPath) shape).lineTo(-119.9707, 102.841805);
        ((GeneralPath) shape).lineTo(-119.9707, 105.623055);
        ((GeneralPath) shape).lineTo(-119.9707, 163.81055);
        ((GeneralPath) shape).lineTo(-17.423828, 163.81055);
        ((GeneralPath) shape).lineTo(-17.423828, 100.064445);
        ((GeneralPath) shape).closePath();
        ((GeneralPath) shape).moveTo(-114.41406, 105.62109);
        ((GeneralPath) shape).lineTo(-22.978516, 105.62109);
        ((GeneralPath) shape).lineTo(-22.978516, 105.62309);
        ((GeneralPath) shape).lineTo(-114.41406, 105.62309);
        ((GeneralPath) shape).closePath();
        ((GeneralPath) shape).moveTo(-108.85938, 111.179695);
        ((GeneralPath) shape).lineTo(-28.535156, 111.179695);
        ((GeneralPath) shape).lineTo(-28.535156, 152.69531);
        ((GeneralPath) shape).lineTo(-108.85938, 152.69531);
        ((GeneralPath) shape).closePath();
        ((GeneralPath) shape).moveTo(-114.41406, 158.25195);
        ((GeneralPath) shape).lineTo(-22.978516, 158.25195);
        ((GeneralPath) shape).lineTo(-22.978516, 158.25395);
        ((GeneralPath) shape).lineTo(-114.41406, 158.25395);
        ((GeneralPath) shape).closePath();

        g.setPaint(WHITE);
        g.fill(shape);

        // _0_1_0_0_0_1_1_0_0_0_1
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(-117.19177, 102.8426);
        ((GeneralPath) shape).lineTo(-117.19177, 105.62262);
        ((GeneralPath) shape).lineTo(-117.19177, 161.03162);
        ((GeneralPath) shape).lineTo(-20.20134, 161.03162);
        ((GeneralPath) shape).lineTo(-20.20134, 102.8426);
        ((GeneralPath) shape).closePath();
        ((GeneralPath) shape).moveTo(-111.63803, 108.40069);
        ((GeneralPath) shape).lineTo(-25.757027, 108.40069);
        ((GeneralPath) shape).lineTo(-25.757027, 155.47351);
        ((GeneralPath) shape).lineTo(-111.63803, 155.47351);
        ((GeneralPath) shape).closePath();

        g.setPaint(new Color(0x323232));
        g.fill(shape);

        // _0_1_0_0_0_1_1_0_0_0_2
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(-114.41278, 105.63626);
        ((GeneralPath) shape).lineTo(-22.929024, 105.63626);
        ((GeneralPath) shape).lineTo(-22.929024, 158.25241);
        ((GeneralPath) shape).lineTo(-114.41278, 158.25241);
        ((GeneralPath) shape).closePath();

        g.setPaint(new Color(0xEEEEEE));
        g.fill(shape);

        g.setTransform(transformations.pop()); // _0_1_0_0_0_1_1_0_0_0

        g.setTransform(transformations.pop()); // _0_1_0_0_0_1_1_0_0
        transformations.push(g.getTransform());
        g.transform(new AffineTransform(1, 0, 0, 1, -4.157296f, -67.20962f));

        // _0_1_0_0_0_1_1_0_1
        transformations.push(g.getTransform());
        g.transform(new AffineTransform(1, 0, 0, 1, 94.80379f, -3.184622f));

        // _0_1_0_0_0_1_1_0_1_0
        shape = new Rectangle2D.Double(-108.8812026977539, 111.16268157958984, 63.86603927612305, 41.526248931884766);
        g.setPaint(WHITE);
        g.fill(shape);
        g.setPaint(new Color(0x323232));
        g.setStroke(new BasicStroke(5.5303283f, 2, 0, 4));
        g.draw(shape);

        g.setTransform(transformations.pop()); // _0_1_0_0_0_1_1_0_1_0

        // _0_1_0_0_0_1_1_0_1_1
        shape = new Rectangle2D.Double(-14.07741928100586, 107.97805786132812, 63.86603927612305, 41.526248931884766);
        g.setPaint(WHITE);
        g.fill(shape);
        transformations.push(g.getTransform());
        g.transform(new AffineTransform(1, 0, 0, 1, 0.04996346f, 1.1567705E-6f));

        // _0_1_0_0_0_1_1_0_1_2
        transformations.push(g.getTransform());
        g.transform(new AffineTransform(0.04047659f, 0, 0, 0.04047659f, 5.9463196f, 108.50491f));

        // _0_1_0_0_0_1_1_0_1_2_0

        // _0_1_0_0_0_1_1_0_1_2_0_0

        // _0_1_0_0_0_1_1_0_1_2_0_0_0
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(667.2, 56.5);
        ((GeneralPath) shape).lineTo(667.2, 55.1);
        ((GeneralPath) shape).curveTo(667.0, 55.1, 666.8, 55.1, 666.60004, 55.1);
        ((GeneralPath) shape).curveTo(664.10004, 55.1, 661.60004, 55.3, 659.2, 55.699997);
        ((GeneralPath) shape).curveTo(655.3, 55.899998, 651.60004, 56.999996, 647.8, 58.899998);
        ((GeneralPath) shape).curveTo(619.6, 70.5, 597.39996, 107.399994, 570.6, 152.09999);
        ((GeneralPath) shape).curveTo(531.3, 217.59999, 482.39996, 299.09998, 398.49997, 319.5);
        ((GeneralPath) shape).curveTo(365.39996, 327.6, 326.3, 374.4, 289.09998, 421.8);
        ((GeneralPath) shape).curveTo(285.69998, 426.09998, 282.59998, 430.09998, 279.8, 433.59998);
        ((GeneralPath) shape).lineTo(278.9, 434.69998);
        ((GeneralPath) shape).lineTo(10.0, 545.9);
        ((GeneralPath) shape).lineTo(228.0, 699.3);
        ((GeneralPath) shape).curveTo(319.5, 656.3, 347.5, 738.6, 423.3, 944.8);
        ((GeneralPath) shape).curveTo(575.8, 927.8, 678.1, 684.1, 763.1, 668.0);
        ((GeneralPath) shape).curveTo(868.5, 648.1, 877.3, 728.8, 990.0, 798.4);
        ((GeneralPath) shape).curveTo(876.8, 553.4, 756.6, 79.4, 667.2, 56.5);
        ((GeneralPath) shape).closePath();
        ((GeneralPath) shape).moveTo(338.9, 615.7);
        ((GeneralPath) shape).lineTo(230.0, 670.9);
        ((GeneralPath) shape).lineTo(60.5, 551.5);
        ((GeneralPath) shape).lineTo(289.0, 457.0);
        ((GeneralPath) shape).lineTo(333.3, 490.0);
        ((GeneralPath) shape).lineTo(401.19998, 541.5);
        ((GeneralPath) shape).curveTo(382.3, 567.8, 361.59998, 592.8, 338.9, 615.7);
        ((GeneralPath) shape).closePath();
        ((GeneralPath) shape).moveTo(415.8, 520.7);
        ((GeneralPath) shape).lineTo(348.19998, 470.40002);
        ((GeneralPath) shape).lineTo(306.69998, 439.00003);
        ((GeneralPath) shape).curveTo(307.19998, 438.30002, 307.8, 437.60004, 308.3, 436.90002);
        ((GeneralPath) shape).curveTo(332.19998, 406.50003, 376.59998, 350.00003, 404.3, 343.30002);
        ((GeneralPath) shape).curveTo(481.4, 324.5, 530.1, 262.8, 568.1, 203.0);
        ((GeneralPath) shape).curveTo(531.5, 295.4, 486.0, 416.3, 415.8, 520.7);
        ((GeneralPath) shape).closePath();

        g.setPaint(new Color(0x323232));
        g.fill(shape);

        g.setTransform(transformations.pop()); // _0_1_0_0_0_1_1_0_1_2_0
        transformations.push(g.getTransform());
        g.transform(new AffineTransform(3.9970944f, 0, 0, 3.9970944f, 161.00748f, 104.52072f));

        // _0_1_0_0_0_1_1_0_1_2_1

        // _0_1_0_0_0_1_1_0_1_2_1_0
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(-37.637775, 3.808492);
        ((GeneralPath) shape).lineTo(-39.886475, 6.057191);
        ((GeneralPath) shape).lineTo(-41.385605, 6.057191);
        ((GeneralPath) shape).lineTo(-39.886475, 4.5580583);
        ((GeneralPath) shape).lineTo(-42.88474, 4.5580583);
        ((GeneralPath) shape).lineTo(-42.88474, 3.0589254);
        ((GeneralPath) shape).lineTo(-39.886475, 3.0589254);
        ((GeneralPath) shape).lineTo(-41.385605, 1.5597926);
        ((GeneralPath) shape).lineTo(-39.886475, 1.5597926);
        ((GeneralPath) shape).closePath();

        g.fill(shape);

        g.setTransform(transformations.pop()); // _0_1_0_0_0_1_1_0_1_2_1

        g.setTransform(transformations.pop()); // _0_1_0_0_0_1_1_0_1_2

        g.setTransform(transformations.pop()); // _0_1_0_0_0_1_1_0_1
        transformations.push(g.getTransform());
        g.transform(new AffineTransform(1, 0, 0, 1, 90.24797f, -53.794605f));

        // _0_1_0_0_0_1_1_0_2
        transformations.push(g.getTransform());
        g.transform(new AffineTransform(1, 0, 0, 1, 0.45535448f, 0));

        // _0_1_0_0_0_1_1_0_2_0
        transformations.push(g.getTransform());
        g.transform(new AffineTransform(1, 0, 0, 1, -1.0588328E-6f, -16.602507f));

        // _0_1_0_0_0_1_1_0_2_0_0

        // _0_1_0_0_0_1_1_0_2_0_0_0
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(-39.585938, 108.40039);
        ((GeneralPath) shape).lineTo(-39.585938, 111.17188);
        ((GeneralPath) shape).lineTo(-39.585938, 122.25195);
        ((GeneralPath) shape).lineTo(-25.697266, 122.25195);
        ((GeneralPath) shape).lineTo(-25.697266, 108.40039);
        ((GeneralPath) shape).closePath();
        ((GeneralPath) shape).moveTo(-34.04492, 113.94336);
        ((GeneralPath) shape).lineTo(-31.240234, 113.94336);
        ((GeneralPath) shape).lineTo(-31.240234, 116.70898);
        ((GeneralPath) shape).lineTo(-34.04492, 116.70898);
        ((GeneralPath) shape).closePath();

        g.fill(shape);

        // _0_1_0_0_0_1_1_0_2_0_0_1
        shape = new Rectangle2D.Double(-36.81454086303711, 111.17204284667969, 8.345879554748535, 8.30827522277832);
        g.setPaint(new Color(0xF80060));
        g.fill(shape);

        g.setTransform(transformations.pop()); // _0_1_0_0_0_1_1_0_2_0_0
        transformations.push(g.getTransform());
        g.transform(new AffineTransform(1, 0, 0, 1, -1.0588328E-6f, 16.60251f));

        // _0_1_0_0_0_1_1_0_2_0_1

        // _0_1_0_0_0_1_1_0_2_0_1_0
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(-39.585938, 108.40039);
        ((GeneralPath) shape).lineTo(-39.585938, 111.17188);
        ((GeneralPath) shape).lineTo(-39.585938, 122.25195);
        ((GeneralPath) shape).lineTo(-25.697266, 122.25195);
        ((GeneralPath) shape).lineTo(-25.697266, 108.40039);
        ((GeneralPath) shape).closePath();
        ((GeneralPath) shape).moveTo(-34.04492, 113.94336);
        ((GeneralPath) shape).lineTo(-31.240234, 113.94336);
        ((GeneralPath) shape).lineTo(-31.240234, 116.70898);
        ((GeneralPath) shape).lineTo(-34.04492, 116.70898);
        ((GeneralPath) shape).closePath();

        g.setPaint(new Color(0x323232));
        g.fill(shape);

        // _0_1_0_0_0_1_1_0_2_0_1_1
        shape = new Rectangle2D.Double(-36.81454086303711, 111.17204284667969, 8.345879554748535, 8.30827522277832);
        g.setPaint(new Color(0xF80060));
        g.fill(shape);

        g.setTransform(transformations.pop()); // _0_1_0_0_0_1_1_0_2_0_1

        // _0_1_0_0_0_1_1_0_2_0_2
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(-39.585938, 108.40039);
        ((GeneralPath) shape).lineTo(-39.585938, 111.17188);
        ((GeneralPath) shape).lineTo(-39.585938, 122.25195);
        ((GeneralPath) shape).lineTo(-25.697266, 122.25195);
        ((GeneralPath) shape).lineTo(-25.697266, 108.40039);
        ((GeneralPath) shape).closePath();
        ((GeneralPath) shape).moveTo(-34.04492, 113.94336);
        ((GeneralPath) shape).lineTo(-31.240234, 113.94336);
        ((GeneralPath) shape).lineTo(-31.240234, 116.70898);
        ((GeneralPath) shape).lineTo(-34.04492, 116.70898);
        ((GeneralPath) shape).closePath();

        g.setPaint(new Color(0x323232));
        g.fill(shape);

        g.setTransform(transformations.pop()); // _0_1_0_0_0_1_1_0_2_0

        // _0_1_0_0_0_1_1_0_2_1
        shape = new Rectangle2D.Double(-36.359188079833984, 111.17204284667969, 8.345879554748535, 8.30827522277832);
        g.setPaint(new Color(0xF80060));
        g.fill(shape);

        g.setTransform(transformations.pop()); // _0_1_0_0_0_1_1_0_2

        g.setTransform(transformations.pop()); // _0_1_0_0_0_1_1

        g.setTransform(transformations.pop()); // _0_1_0_0_0_1

        g.setTransform(transformations.pop()); // _0_1_0_0_0

        g.setTransform(transformations.pop()); // _0_1_0_0

        g.setTransform(transformations.pop()); // _0_1_0

        g.setTransform(transformations.pop()); // _0_1

    }
}
