package com.velasolaris.plugin.painter;

import com.velasolaris.plugin.controller.spi.IPluginImagePainter;

import java.awt.*;
import java.awt.geom.AffineTransform;
import java.awt.geom.GeneralPath;
import java.awt.geom.Rectangle2D;

import static java.awt.Color.WHITE;

/**
 * Paints the Matlab MCR plugin controller in the Polysun designer.
 */
public class MCRPluginControllerPainter implements IPluginImagePainter {

    @Override
    public void paint(Graphics2D g) {
        Shape shape = null;

        float origAlpha = 1.0f;
        Composite origComposite = g.getComposite();
        if (origComposite instanceof AlphaComposite) {
            AlphaComposite origAlphaComposite = (AlphaComposite)origComposite;
            if (origAlphaComposite.getRule() == AlphaComposite.SRC_OVER) {
                origAlpha = origAlphaComposite.getAlpha();
            }
        }

        java.util.LinkedList<AffineTransform> transformations = new java.util.LinkedList<AffineTransform>();


        //

        // _0
        transformations.push(g.getTransform());
        g.transform(new AffineTransform(1, 0, 0, 1, 31.072315f, -38.340137f));

        // _0_0

        g.setTransform(transformations.pop()); // _0_0
        transformations.push(g.getTransform());
        g.transform(new AffineTransform(0.90677965f, 0, 0, 0.90677965f, 12.552985f, -38.65011f));

        // _0_1
        transformations.push(g.getTransform());
        g.transform(new AffineTransform(1, 0, 0, 1, 19.028038f, 1.4636421f));

        // _0_1_0
        transformations.push(g.getTransform());
        g.transform(new AffineTransform(0.39790422f, 0, 0, 0.39790422f, 0.83772373f, 1.7224818f));

        // _0_1_0_0
        transformations.push(g.getTransform());
        g.transform(new AffineTransform(1, 0, 0, 1, 30.69656f, -0.72493494f));

        // _0_1_0_0_0
        transformations.push(g.getTransform());
        g.transform(new AffineTransform(0, 0.971383f, -0.9713829f, 0, -13.19697f, 62.049095f));

        // _0_1_0_0_0_0

        g.setTransform(transformations.pop()); // _0_1_0_0_0_0
        transformations.push(g.getTransform());
        g.transform(new AffineTransform(1, 0, 0, 1, 4.5428276f, -0.255567f));

        // _0_1_0_0_0_1
        transformations.push(g.getTransform());
        g.transform(new AffineTransform(0.7497474f, 0, 0, 0.7509831f, -82.78321f, 65.3706f));

        // _0_1_0_0_0_1_0

        g.setTransform(transformations.pop()); // _0_1_0_0_0_1_0
        transformations.push(g.getTransform());
        g.transform(new AffineTransform(1, 0, 0, 1, -90.646484f, 70.39424f));

        // _0_1_0_0_0_1_1

        // _0_1_0_0_0_1_1_0
        transformations.push(g.getTransform());
        g.transform(new AffineTransform(1, 0, 0, 1, 90.646484f, -70.39424f));

        // _0_1_0_0_0_1_1_0_0
        transformations.push(g.getTransform());
        g.transform(new AffineTransform(1, 0, 0, 1, 0.01350987f, 0.02869167f));

        // _0_1_0_0_0_1_1_0_0_0

        // _0_1_0_0_0_1_1_0_0_0_0
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(-119.9707, 100.06445);
        ((GeneralPath) shape).lineTo(-119.9707, 102.841805);
        ((GeneralPath) shape).lineTo(-119.9707, 105.623055);
        ((GeneralPath) shape).lineTo(-119.9707, 163.81055);
        ((GeneralPath) shape).lineTo(-17.423828, 163.81055);
        ((GeneralPath) shape).lineTo(-17.423828, 100.064445);
        ((GeneralPath) shape).closePath();
        ((GeneralPath) shape).moveTo(-114.41406, 105.62109);
        ((GeneralPath) shape).lineTo(-22.978516, 105.62109);
        ((GeneralPath) shape).lineTo(-22.978516, 105.62309);
        ((GeneralPath) shape).lineTo(-114.41406, 105.62309);
        ((GeneralPath) shape).closePath();
        ((GeneralPath) shape).moveTo(-108.85938, 111.179695);
        ((GeneralPath) shape).lineTo(-28.535156, 111.179695);
        ((GeneralPath) shape).lineTo(-28.535156, 152.69531);
        ((GeneralPath) shape).lineTo(-108.85938, 152.69531);
        ((GeneralPath) shape).closePath();
        ((GeneralPath) shape).moveTo(-114.41406, 158.25195);
        ((GeneralPath) shape).lineTo(-22.978516, 158.25195);
        ((GeneralPath) shape).lineTo(-22.978516, 158.25395);
        ((GeneralPath) shape).lineTo(-114.41406, 158.25395);
        ((GeneralPath) shape).closePath();

        g.setPaint(WHITE);
        g.fill(shape);

        // _0_1_0_0_0_1_1_0_0_0_1
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(-117.19177, 102.8426);
        ((GeneralPath) shape).lineTo(-117.19177, 105.62262);
        ((GeneralPath) shape).lineTo(-117.19177, 161.03162);
        ((GeneralPath) shape).lineTo(-20.20134, 161.03162);
        ((GeneralPath) shape).lineTo(-20.20134, 102.8426);
        ((GeneralPath) shape).closePath();
        ((GeneralPath) shape).moveTo(-111.63803, 108.40069);
        ((GeneralPath) shape).lineTo(-25.757034, 108.40069);
        ((GeneralPath) shape).lineTo(-25.757034, 155.47351);
        ((GeneralPath) shape).lineTo(-111.63803, 155.47351);
        ((GeneralPath) shape).closePath();

        g.setPaint(new Color(0x323232));
        g.fill(shape);

        // _0_1_0_0_0_1_1_0_0_0_2
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(-114.41278, 105.63626);
        ((GeneralPath) shape).lineTo(-22.929024, 105.63626);
        ((GeneralPath) shape).lineTo(-22.929024, 158.25241);
        ((GeneralPath) shape).lineTo(-114.41278, 158.25241);
        ((GeneralPath) shape).closePath();

        g.setPaint(new Color(0xEEEEEE));
        g.fill(shape);

        g.setTransform(transformations.pop()); // _0_1_0_0_0_1_1_0_0_0

        g.setTransform(transformations.pop()); // _0_1_0_0_0_1_1_0_0
        transformations.push(g.getTransform());
        g.transform(new AffineTransform(1, 0, 0, 1, -4.157296f, -67.20962f));

        // _0_1_0_0_0_1_1_0_1
        transformations.push(g.getTransform());
        g.transform(new AffineTransform(1, 0, 0, 1, -113.17661f, 6.1855183f));

        // _0_1_0_0_0_1_1_0_1_0

        // _0_1_0_0_0_1_1_0_1_0_0
        shape = new Rectangle2D.Double(101.91676330566406, 105.89893341064453, 30.304529190063477, 8.354491233825684);
        g.setPaint(WHITE);
        g.fill(shape);
        g.setPaint(new Color(0x323232));
        g.setStroke(new BasicStroke(5.434972f, 2, 0, 4));
        g.draw(shape);

        g.setTransform(transformations.pop()); // _0_1_0_0_0_1_1_0_1_0

        g.setTransform(transformations.pop()); // _0_1_0_0_0_1_1_0_1
        transformations.push(g.getTransform());
        g.transform(new AffineTransform(1, 0, 0, 1, 65.64997f, -66.27409f));

        // _0_1_0_0_0_1_1_0_2

        // _0_1_0_0_0_1_1_0_2_0
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(-39.585938, 108.40039);
        ((GeneralPath) shape).lineTo(-39.585938, 111.17188);
        ((GeneralPath) shape).lineTo(-39.585938, 122.25195);
        ((GeneralPath) shape).lineTo(-25.697266, 122.25195);
        ((GeneralPath) shape).lineTo(-25.697266, 108.40039);
        ((GeneralPath) shape).closePath();
        ((GeneralPath) shape).moveTo(-34.04492, 113.94336);
        ((GeneralPath) shape).lineTo(-31.240234, 113.94336);
        ((GeneralPath) shape).lineTo(-31.240234, 116.70898);
        ((GeneralPath) shape).lineTo(-34.04492, 116.70898);
        ((GeneralPath) shape).closePath();

        g.fill(shape);

        // _0_1_0_0_0_1_1_0_2_1
        shape = new Rectangle2D.Double(-36.81454086303711, 111.17204284667969, 8.345879554748535, 8.30827522277832);
        g.setPaint(new Color(0xF80060));
        g.fill(shape);

        g.setTransform(transformations.pop()); // _0_1_0_0_0_1_1_0_2
        transformations.push(g.getTransform());
        g.transform(new AffineTransform(1, 0, 0, 1, -29.088974f, -20.8085f));

        // _0_1_0_0_0_1_1_0_3
        transformations.push(g.getTransform());
        g.transform(new AffineTransform(1, 0, 0, 1, 116.91522f, -45.46559f));

        // _0_1_0_0_0_1_1_0_3_0
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(-39.585938, 108.40039);
        ((GeneralPath) shape).lineTo(-39.585938, 111.17188);
        ((GeneralPath) shape).lineTo(-39.585938, 122.25195);
        ((GeneralPath) shape).lineTo(-25.697266, 122.25195);
        ((GeneralPath) shape).lineTo(-25.697266, 108.40039);
        ((GeneralPath) shape).closePath();
        ((GeneralPath) shape).moveTo(-34.04492, 113.94336);
        ((GeneralPath) shape).lineTo(-31.240234, 113.94336);
        ((GeneralPath) shape).lineTo(-31.240234, 116.70898);
        ((GeneralPath) shape).lineTo(-34.04492, 116.70898);
        ((GeneralPath) shape).closePath();

        g.setPaint(new Color(0x323232));
        g.fill(shape);

        g.setTransform(transformations.pop()); // _0_1_0_0_0_1_1_0_3_0

        // _0_1_0_0_0_1_1_0_3_1
        shape = new Rectangle2D.Double(80.1006851196289, 65.70645141601562, 8.345879554748535, 8.30827522277832);
        g.setPaint(new Color(0xF80060));
        g.fill(shape);

        g.setTransform(transformations.pop()); // _0_1_0_0_0_1_1_0_3
        transformations.push(g.getTransform());
        g.transform(new AffineTransform(1, 0, 0, 1, -4.157296f, -67.20962f));

        // _0_1_0_0_0_1_1_0_4
        transformations.push(g.getTransform());
        g.transform(new AffineTransform(1, 0, 0, 1, -113.17661f, 6.1855183f));

        // _0_1_0_0_0_1_1_0_4_0

        // _0_1_0_0_0_1_1_0_4_0_0
        shape = new Rectangle2D.Double(101.91676330566406, 105.89893341064453, 30.304529190063477, 8.354491233825684);
        g.setPaint(WHITE);
        g.fill(shape);

        g.setTransform(transformations.pop()); // _0_1_0_0_0_1_1_0_4_0

        g.setTransform(transformations.pop()); // _0_1_0_0_0_1_1_0_4
        transformations.push(g.getTransform());
        g.transform(new AffineTransform(3.9970944f, 0, 0, 3.9970944f, 167.34319f, 55.19039f));

        // _0_1_0_0_0_1_1_0_5

        // _0_1_0_0_0_1_1_0_5_0
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(-37.637775, 3.808492);
        ((GeneralPath) shape).lineTo(-39.886475, 6.057191);
        ((GeneralPath) shape).lineTo(-41.385605, 6.057191);
        ((GeneralPath) shape).lineTo(-39.886475, 4.5580583);
        ((GeneralPath) shape).lineTo(-42.88474, 4.5580583);
        ((GeneralPath) shape).lineTo(-42.88474, 3.0589254);
        ((GeneralPath) shape).lineTo(-39.886475, 3.0589254);
        ((GeneralPath) shape).lineTo(-41.385605, 1.5597926);
        ((GeneralPath) shape).lineTo(-39.886475, 1.5597926);
        ((GeneralPath) shape).closePath();

        g.setPaint(new Color(0x323232));
        g.fill(shape);

        g.setTransform(transformations.pop()); // _0_1_0_0_0_1_1_0_5
        transformations.push(g.getTransform());
        g.transform(new AffineTransform(0.02726023f, 0, 0, 0.02726023f, 18.544725f, 56.78454f));

        // _0_1_0_0_0_1_1_0_6

        // _0_1_0_0_0_1_1_0_6_0

        // _0_1_0_0_0_1_1_0_6_0_0
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(667.2, 56.5);
        ((GeneralPath) shape).lineTo(667.2, 55.1);
        ((GeneralPath) shape).curveTo(667.0, 55.1, 666.8, 55.1, 666.60004, 55.1);
        ((GeneralPath) shape).curveTo(664.10004, 55.1, 661.60004, 55.3, 659.2, 55.699997);
        ((GeneralPath) shape).curveTo(655.3, 55.899998, 651.60004, 56.999996, 647.8, 58.899998);
        ((GeneralPath) shape).curveTo(619.6, 70.5, 597.39996, 107.399994, 570.6, 152.09999);
        ((GeneralPath) shape).curveTo(531.3, 217.59999, 482.39996, 299.09998, 398.49997, 319.5);
        ((GeneralPath) shape).curveTo(365.39996, 327.6, 326.3, 374.4, 289.09998, 421.8);
        ((GeneralPath) shape).curveTo(285.69998, 426.09998, 282.59998, 430.09998, 279.8, 433.59998);
        ((GeneralPath) shape).lineTo(278.9, 434.69998);
        ((GeneralPath) shape).lineTo(10.0, 545.9);
        ((GeneralPath) shape).lineTo(228.0, 699.3);
        ((GeneralPath) shape).curveTo(319.5, 656.3, 347.5, 738.6, 423.3, 944.8);
        ((GeneralPath) shape).curveTo(575.8, 927.8, 678.1, 684.1, 763.1, 668.0);
        ((GeneralPath) shape).curveTo(868.5, 648.1, 877.3, 728.8, 990.0, 798.4);
        ((GeneralPath) shape).curveTo(876.8, 553.4, 756.6, 79.4, 667.2, 56.5);
        ((GeneralPath) shape).closePath();
        ((GeneralPath) shape).moveTo(338.9, 615.7);
        ((GeneralPath) shape).lineTo(230.0, 670.9);
        ((GeneralPath) shape).lineTo(60.5, 551.5);
        ((GeneralPath) shape).lineTo(289.0, 457.0);
        ((GeneralPath) shape).lineTo(333.3, 490.0);
        ((GeneralPath) shape).lineTo(401.19998, 541.5);
        ((GeneralPath) shape).curveTo(382.3, 567.8, 361.59998, 592.8, 338.9, 615.7);
        ((GeneralPath) shape).closePath();
        ((GeneralPath) shape).moveTo(415.8, 520.7);
        ((GeneralPath) shape).lineTo(348.19998, 470.40002);
        ((GeneralPath) shape).lineTo(306.69998, 439.00003);
        ((GeneralPath) shape).curveTo(307.19998, 438.30002, 307.8, 437.60004, 308.3, 436.90002);
        ((GeneralPath) shape).curveTo(332.19998, 406.50003, 376.59998, 350.00003, 404.3, 343.30002);
        ((GeneralPath) shape).curveTo(481.4, 324.5, 530.1, 262.8, 568.1, 203.0);
        ((GeneralPath) shape).curveTo(531.5, 295.4, 486.0, 416.3, 415.8, 520.7);
        ((GeneralPath) shape).closePath();

        g.fill(shape);

        g.setTransform(transformations.pop()); // _0_1_0_0_0_1_1_0_6
        transformations.push(g.getTransform());
        g.transform(new AffineTransform(1.1608803f, 0, 0, 1.1608803f, 34.6214f, 62.41792f));

        // _0_1_0_0_0_1_1_0_7

        // _0_1_0_0_0_1_1_0_7_0
        shape = new GeneralPath();
        ((GeneralPath) shape).moveTo(7.206863, 7.7966514);
        ((GeneralPath) shape).curveTo(7.081863, 7.7446513, 6.9528627, 7.7216516, 6.826863, 7.6826515);
        ((GeneralPath) shape).lineTo(6.522863, 5.8616514);
        ((GeneralPath) shape).lineTo(4.522863, 5.8616514);
        ((GeneralPath) shape).lineTo(4.219863, 7.6796513);
        ((GeneralPath) shape).curveTo(3.966863, 7.7586513, 3.721863, 7.859651, 3.487863, 7.982651);
        ((GeneralPath) shape).lineTo(1.9878632, 6.911651);
        ((GeneralPath) shape).lineTo(0.5728632, 8.325651);
        ((GeneralPath) shape).lineTo(1.6458632, 9.827651);
        ((GeneralPath) shape).curveTo(1.5838631, 9.945651, 1.5098631, 10.05265, 1.4578632, 10.17765);
        ((GeneralPath) shape).curveTo(1.4058632, 10.30265, 1.3828632, 10.43165, 1.3438632, 10.557651);
        ((GeneralPath) shape).lineTo(-0.47713685, 10.86165);
        ((GeneralPath) shape).lineTo(-0.47713685, 12.86165);
        ((GeneralPath) shape).lineTo(1.3408632, 13.164651);
        ((GeneralPath) shape).curveTo(1.4198632, 13.417651, 1.5208632, 13.662651, 1.6438632, 13.896651);
        ((GeneralPath) shape).lineTo(0.5728632, 15.396651);
        ((GeneralPath) shape).lineTo(1.9868631, 16.810652);
        ((GeneralPath) shape).lineTo(3.488863, 15.737652);
        ((GeneralPath) shape).curveTo(3.606863, 15.799652, 3.713863, 15.8736515, 3.838863, 15.925652);
        ((GeneralPath) shape).curveTo(3.963863, 15.977652, 4.0918627, 16.000652, 4.218863, 16.039652);
        ((GeneralPath) shape).lineTo(4.522863, 17.861652);
        ((GeneralPath) shape).lineTo(6.522863, 17.861652);
        ((GeneralPath) shape).lineTo(6.825863, 16.043652);
        ((GeneralPath) shape).curveTo(7.0788627, 15.964651, 7.323863, 15.863651, 7.5578628, 15.740651);
        ((GeneralPath) shape).lineTo(9.057863, 16.811651);
        ((GeneralPath) shape).lineTo(10.471864, 15.397652);
        ((GeneralPath) shape).lineTo(9.398864, 13.895652);
        ((GeneralPath) shape).curveTo(9.460864, 13.777652, 9.534863, 13.670651, 9.5868635, 13.545651);
        ((GeneralPath) shape).curveTo(9.638864, 13.420651, 9.661863, 13.292651, 9.700864, 13.165651);
        ((GeneralPath) shape).lineTo(11.522864, 12.861651);
        ((GeneralPath) shape).lineTo(11.522864, 10.861651);
        ((GeneralPath) shape).lineTo(9.7048645, 10.558651);
        ((GeneralPath) shape).curveTo(9.626209, 10.30599, 9.524794, 10.060986, 9.401864, 9.826652);
        ((GeneralPath) shape).lineTo(10.472864, 8.326652);
        ((GeneralPath) shape).lineTo(9.058864, 6.9116516);
        ((GeneralPath) shape).lineTo(7.556864, 7.9846516);
        ((GeneralPath) shape).curveTo(7.4388638, 7.922652, 7.331864, 7.8486514, 7.206864, 7.7966514);
        ((GeneralPath) shape).closePath();
        ((GeneralPath) shape).moveTo(7.418863, 11.86165);
        ((GeneralPath) shape).curveTo(7.418863, 12.908782, 6.569995, 13.75765, 5.522863, 13.75765);
        ((GeneralPath) shape).curveTo(4.475731, 13.75765, 3.6268628, 12.908782, 3.6268628, 11.86165);
        ((GeneralPath) shape).curveTo(3.6268628, 10.814519, 4.475731, 9.965651, 5.522863, 9.965651);
        ((GeneralPath) shape).curveTo(6.569995, 9.965651, 7.418863, 10.814519, 7.418863, 11.86165);
        ((GeneralPath) shape).closePath();

        g.setPaint(new Color(0x00BF77));
        g.fill(shape);

        g.setTransform(transformations.pop()); // _0_1_0_0_0_1_1_0_7

        g.setTransform(transformations.pop()); // _0_1_0_0_0_1_1

        g.setTransform(transformations.pop()); // _0_1_0_0_0_1

        g.setTransform(transformations.pop()); // _0_1_0_0_0

        g.setTransform(transformations.pop()); // _0_1_0_0

        g.setTransform(transformations.pop()); // _0_1_0

        g.setTransform(transformations.pop()); // _0_1

    }
}
