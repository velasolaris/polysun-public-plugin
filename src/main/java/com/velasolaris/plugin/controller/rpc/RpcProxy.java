package com.velasolaris.plugin.controller.rpc;

import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;

/**
 * Base RPC proxy class encapsulating RPC communication.
 *
 * @author rkurmann
 * @since Polysun 9.1
 *
 */
abstract class RpcProxy {

    /** Wait forever until the connection to the server is successful. */
    public static final int WAIT_CONNECTED_FOREVER = -1;
    /** No waiting until the connection to the server is successful. Fail immediately. */
    public static final int WAIT_CONNECTED_NO_WAIT = 0;

    /** Static instance of the Logger for this class */
    protected static Logger sLog = Logger.getLogger(RpcProxy.class.getName());

    /** Name of the RPC function to call. Comes from the controller element GUI. */
    protected String rpcFunction;

    /** URL of the webservice having the RPC control functions. */
    protected URL rpcServerURL = null;

    /** Connection timeout [ms]. 0 may mean wait forever. */
    protected int connectionTimeout = 10000;

    /** Read timeout [ms]. 0 may mean wait forever. */
    protected int readTimeout = 10000;

    /**
     * Verbose level.
     * 0 = default
     * 1 = verbose
     * 2 = debug
     * Comes from the controller element GUI.
     *
     * @see SimpleRpcPluginController#VERBOSE_LEVEL_STANDARD
     * @see SimpleRpcPluginController#VERBOSE_LEVEL_VERBOSE
     * @see SimpleRpcPluginController#VERBOSE_LEVEL_DEBUG
     */
    protected int verboseLevel;

    /** Empty paramters object. */
    final protected Map<String, Object> emptyParamters = new HashMap<>();

    /**
     * Constructor.
     *
     * @param rpcServerURL URL of the RPC server for the function calls, e.g. http://localhost:2102/control
     * @param rpcFunction Name of the RPC function, e.g. controlFlowrate
     * @param connectionTimeout Connection timeout [ms] 0 may mean wait forever.
     * @param readTimeout Read timeout [ms] 0 may mean wait forever.
     * @param verboseLevel Level of verbosity
     */
    protected RpcProxy(URL rpcServerURL, String rpcFunction, int connectionTimeout, int readTimeout, int verboseLevel) {
        this.rpcServerURL = rpcServerURL;
        this.rpcFunction = rpcFunction;
        this.connectionTimeout = connectionTimeout;
        this.readTimeout = readTimeout;
    }

    /**
     * Calls the remote function.
     *
     * @param simulationTime
     *            simulationTime, int: The simulation time in [s] beginning from
     *            the 1. January 00:00 (no leap year).
     * @param status
     *            0/1: The status of this controller according to user settings,
     *            1 means enabled, 0 disabled. The status originates from the
     *            timer setting of the controller dialog. The user can enable or
     *            disable the controller for certain hours, days or month. This
     *            value should be respected by the controller implementation,
     *            otherwise it could lead to an unexpected user experience.
     * @param sensors
     *            float vector: The values of the sensors configured by the user
     *            (Input parameter) (length is available during init stage = 0)
     * @param sensorsUsed
     *            vector 0/1: 1 indicates that the sensor is used in Polysun
     *            (available during init stage = 0)
     * @param properties
     *            float vector: The properties set in Polysun (available during
     *            init stage = 0)
     * @param propertiesStr
     *            String vector: The properties set in Polysun as string array
     *            (available during init stage = 0)
     * @param preRun
     *            0/1: Is this the real simulation or a pre run phase? This
     *            value can be ignored.
     * @param controlSignalsUsed
     *            float vector: 1 indicates that the control signal is used in
     *            Polysun (available during init stage = 0)
     * @param logValues
     *            float vector: The log values that can be returned.
     *            Configurable in Polysun.
     * @param stage
     *            int: Stage of the function call 0 = Init, called before the
     *            simulation to init (initSimulation), results will be ignored 1
     *            = during the simulation (simulation), 2 = after the simulation
     *            (terminateSimulation), results will be ignored
     * @param fixedTimestep
     *            int: Fixed timestep. For each timepoint which is a multiple of
     *            this fixedTimestep, the simulation does a timestep. The
     *            Polysun solver can do more timesteps if necessary. Example,
     *            for fixedTimestep of 180s, the simulation solver does a
     *            simulation at least at 0s, 180s, 360s, 480s, 720s, ... 0 means
     *            no fixed timestep and Polysun uses the default timesteps (240s
     *            during the day and 720s during the night).
     * @param verboseLevel
     *            int: How much output should this function display to the
     *            console? (available during init stage = 0) 0 = standard output
     *            1 = verbose output 2 = debug output
     * @param parameters
     *            Map&lt;String, Object&gt;: Generic parameters
     * @return controlSignals The control signals set by this plugin controller
     *         (Output parameter). logValues: The values to log in Polysun, e.g
     *         intermediate results. This value can be ignored. These values are
     *         shown in the Simulation Analysis or in the Log and Parameterizing
     *         output. timepoints [s]. Registers these timepoints in the future,
     *         where the simulation have to do a timestep. It doesn't matter, if
     *         the same timepoint will be registered several times. Timepoint in
     *         the array is in seconds from the 1. Jan. 00:00, or
     *         <code>null</code> if no additional timesteps are required. These
     *         timesteps can be used for time based controlling strategies.
     *
     * @throws Exception
     *             For any problems
     */
    public abstract ControlFunctionResponse callRemoteFunction(int simulationTime, boolean status, float[] sensors,
            boolean[] sensorsUsed, float[] properties, String[] propertiesStr, boolean preRun,
            boolean[] controlSignalsUsed, float[] logValues, int stage, int fixedTimestep, int verboseLevel,
            Map<String, Object> parameters) throws Exception;

    /**
     * Converts float[] to a Double[].
     *
     * @param input input array
     * @return output array
     */
    protected Double[] convertFloatsToDoubles(float[] input) {
        if (input == null) {
            return null;
        }
        Double[] output = new Double[input.length];
        for (int i = 0; i < input.length; i++) {
            output[i] = (double) input[i];
        }
        return output;
    }

    /**
     * Disconnects the RPC proxy.
     */
    public abstract void disconnectProxy();

    /**
     * Returns the verbose level.
     *
     * @return the verboseLevel
     */
    public int getVerboseLevel() {
        return verboseLevel;
    }

    /**
     * Sets up the RPC proxy.
     * @param waitConnected Time to wait until the server is ready to be connected [ms]. -1 = wait forever, 0 = no waiting
     * @param parameters Generic parameters
     * @throws Exception For any problems
     */
    public abstract void setupRpc(int waitConnected, Map<String, Object> parameters) throws Exception;

    /**
     * Sets the verbose level
     * @param verboseLevel
     *            the verboseLevel to set
     */
    public void setVerboseLevel(int verboseLevel) {
        this.verboseLevel = verboseLevel;
    }

    /**
     * Writes a message to the RPC Server console.
     * @param str message to write to the server
     *
     * @throws Exception  For any problems
     */
    public abstract void writeMsgToServer(String str) throws Exception;

    /**
     * Pings the server.
     *
     * @return <code>true</code> if the server responded with <code>poong</code>, otherwise <code>false</code>.
     */
    public abstract boolean ping();

    /**
     * Waits until the server is up and connected.
     *
     * @param timeout Time to wait until the server is ready to be connected [ms]. -1 = wait forever, 0 = no waiting
     * @return <code>true</code> if the server responded with <code>poong</code>, otherwise <code>false</code>.
     */
    public boolean waitConnected(int timeout) {
        long start = System.currentTimeMillis();
        while ((start + timeout > System.currentTimeMillis() || timeout == WAIT_CONNECTED_FOREVER) && !ping()) {
            try {
                Thread.sleep(25); // Wait some time
            } catch (InterruptedException e) {
                // ignore
            }
        }
        sLog.fine("Waited for server connect " + (System.currentTimeMillis() - start) + "ms (timeout = " + timeout + "ms)");
        return ping();
    }

    /**
     * Stop server (gracefully).
     *
     * @throws Exception  For any problems
     */
    public abstract void stopServer() throws Exception;


}
