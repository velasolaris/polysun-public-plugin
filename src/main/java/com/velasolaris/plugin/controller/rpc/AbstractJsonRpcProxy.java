package com.velasolaris.plugin.controller.rpc;

import static com.velasolaris.plugin.controller.rpc.ControlFunctionResponse.EMPTY_FLOAT_ARRAY;
import static com.velasolaris.plugin.controller.rpc.ControlFunctionResponse.EMPTY_INT_ARRAY;
import static com.velasolaris.plugin.controller.rpc.ControlFunctionResponse.convertObjectArrayToFloats;
import static com.velasolaris.plugin.controller.rpc.ControlFunctionResponse.convertObjectToInts;

import java.net.URL;
import java.util.Arrays;
import java.util.Map;

import org.apache.commons.lang3.ArrayUtils;

import com.thetransactioncompany.jsonrpc2.JSONRPC2Request;
import com.thetransactioncompany.jsonrpc2.JSONRPC2Response;
import com.velasolaris.plugin.controller.spi.PluginControllerException;

import net.minidev.json.JSONArray;

/**
 * Common base class for JSON-RPC proxies.
 *
 * @author rkurmann
 * @since 9.2
 *
 */
public abstract class AbstractJsonRpcProxy extends RpcProxy {

    /**
     * Constructor.
     *
     * @param rpcServerURL URL of the RPC server for the function calls, e.g. http://localhost:2102/control
     * @param rpcFunction Name of the RPC function, e.g. controlFlowrate
     * @param connectionTimeout Connection timeout [ms] 0 may mean wait forever.
     * @param readTimeout Read timeout [ms] 0 may mean wait forever.
     * @param verboseLevel Level of verbosity
     */
    protected AbstractJsonRpcProxy(URL rpcServerURL, String rpcFunction, int connectionTimeout, int readTimeout, int verboseLevel) {
        super(rpcServerURL, rpcFunction, connectionTimeout, readTimeout, verboseLevel);
    }

    /**
     * Invoke a RPC function via JSON-RPC over the TCP socket.
     *
     * @param request the JSON-RPC request
     * @return the JSON-RPC response
     * @throws Exception a for problems
     */
    protected abstract JSONRPC2Response invoke(JSONRPC2Request request) throws Exception;

    @Override
    public ControlFunctionResponse callRemoteFunction(int simulationTime, boolean status, float[] sensors, boolean[] sensorsUsed, float[] properties, String[] propertiesStr,
            boolean preRun, boolean[] controlSignalsUsed, float[] logValues, int stage, int fixedTimestep, int verboseLevel, Map<String, Object> parameters)
        throws Exception {
        // def control(simulationTime, status, sensors, sensorsUsed, properties,
    // propertiesStr, preRun, controlSignalsUsed, numLogValues, stage,
// fixedTimestep, verboseLevel, parameters)
        JSONRPC2Request request = new JSONRPC2Request(rpcFunction,
                Arrays.asList(new Object[] { simulationTime, status, convertFloatsToDoubles(sensors),
                    ArrayUtils.toObject(sensorsUsed), convertFloatsToDoubles(properties), propertiesStr, preRun,
                    ArrayUtils.toObject(controlSignalsUsed), logValues.length, stage, fixedTimestep, verboseLevel,
                    /*parameters != null && false ? parameters :*/ emptyParamters }),
                simulationTime);
        JSONRPC2Response response = invoke(request);
        ControlFunctionResponse result;
        if (response.indicatesSuccess()) {
            if (response.getResult() instanceof JSONArray) {
                Object[] resultArray = ((JSONArray) response.getResult()).toArray();
                result = new ControlFunctionResponse(
                        resultArray.length > 0 ? convertObjectArrayToFloats(resultArray[0]) : EMPTY_FLOAT_ARRAY,
                        resultArray.length > 1 ? convertObjectArrayToFloats(resultArray[1]) : EMPTY_FLOAT_ARRAY,
                        resultArray.length > 2 ? convertObjectToInts(resultArray[2]) : EMPTY_INT_ARRAY);
            } else {
                result = new ControlFunctionResponse();
            }
        } else {
            throw new PluginControllerException(response.getError());
        }
        return result;
    }

    @Override
    public void setupRpc(int waitConnected, Map<String, Object> parameters) throws Exception {
        waitConnected(waitConnected);
        writeMsgToServer("Polysun connected to Server.");
        sLog.info("Connected to Server");
    }

    @Override
    public void writeMsgToServer(String str) throws Exception {
        if (verboseLevel >= SimpleRpcPluginController.VERBOSE_LEVEL_DEBUG) {
            sLog.fine("Write Message to Server: " + str);
        }
        invoke(new JSONRPC2Request("print", Arrays.asList(new Object[] { str }), 0));
    }

    @Override
    public boolean ping() {
        try {
            JSONRPC2Response response = invoke(new JSONRPC2Request("ping", 0));
            return response.indicatesSuccess() && "pong".equals(response.getResult());
        } catch (Exception e) {
            return false;
        }
    }

    @Override
    public void stopServer() throws Exception {
        invoke(new JSONRPC2Request("stop", 0));
    }

}
