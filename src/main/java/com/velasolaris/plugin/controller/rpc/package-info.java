/**
 * Package containing the SimpleRpcPluginController delegating to XML-RPC or JSON-RPC servers.
 *
 * @see com.velasolaris.plugin.controller.rpc.SimpleRpcPluginController
 */
package com.velasolaris.plugin.controller.rpc;
