package com.velasolaris.plugin.controller.matlab.matconsolectl;

import static com.velasolaris.plugin.controller.spi.PluginControllerConfiguration.DEFAULT_TIMESTEP;

import java.util.Map;


import com.velasolaris.plugin.controller.spi.AbstractPluginController;
import com.velasolaris.plugin.controller.spi.PluginControllerException;
import com.velasolaris.plugin.controller.spi.PolysunSettings;

/**
 * Base class for the Matlab and MCR plugin controllers
 * @author MAJ
 * @since Polysun 11.1
 */
public abstract class AbstractMatlabPluginController extends AbstractPluginController {

    /** Property name for verbose in the controller element GUI. */
    protected static final String PROPERTY_VERBOSE_LEVEL = "Verbose level";
    /** Property name for fixed timestep in the controller element GUI. */
    protected static final String PROPERTY_FIXED_TIMESTEP = "Fixed timestep";
    /** Property name for number of generic sensors in the controller element GUI. */
    protected static final String PROPERTY_NUM_GENERIC_SENSORS = "Number of sensors";
    /** Property name for number of generic control signals in the controller element GUI. */
    protected static final String PROPERTY_NUM_GENERIC_CONTROL_SIGNALS = "Number of controls signals";
    /** Property name for number of generic log values in the controller element GUI. */
    protected static final String PROPERTY_NUM_GENERIC_LOGS = "Number of logs";

    /** Standard verbose level. */
    public static final int VERBOSE_LEVEL_STANDARD = 0;
    /** Verbose level: Verbose (more thans standard output). */
    public static final int VERBOSE_LEVEL_VERBOSE = 1;
    /** Debug verbose level. */
    public static final int VERBOSE_LEVEL_DEBUG = 2;
    /** Call before simulation, i.e. initaliseSimulation() */
    public static final int FUNCTION_STAGE_INIT = 0;
    /** Call during simulation */
    public static final int FUNCTION_STAGE_SIMULATION = 1;
    /** Call after simulation, i.e. initaliseSimulation() */
    public static final int FUNCTION_STAGE_TERMINATE = 2;

    /**
     * Verbose level.
     * 0 = default
     * 1 = verbose
     * 2 = debug
     * Comes from the controller element GUI.
     *
     * @see VERBOSE_LEVEL_STANDARD
     * @see VERBOSE_LEVEL_VERBOSE
     * @see VERBOSE_LEVEL_DEBUG
     */
    protected int verboseLevel;

    /**
     * Fixed timestep. For each timepoint which is a multiple of this
     * fixedTimestep, the simulation does a timestep. The Polysun solver can do
     * more timesteps if necessary. Example, for fixedTimestep of 180s, the
     * simulation solver does a simulation at least at 0s, 180s, 360s, 480s,
     * 720s, ...
     * 0 means no fixed timestep and Polysun uses the default timesteps
     * (240s during the day and 720s during the night).
     */
    protected int fixedTimestep = DEFAULT_TIMESTEP;

    /** Properties that will be passed to Matlab. Properties that are used for the MatlabPluginController itself are removed.*/
    protected float[] matlabPropertiesFloat;
    /** Properties as String that will be passed to Matlab. Properties that are used for the MatlabPluginController itself are removed.*/
    protected String[] matlabPropertiesString;
    /** Number of configured properties. These number of properties will be removed from properties to create <code>matlabPropertiesFloat</code> and <code>matlabPropertiesString</code>. */
    protected int numInternProperties;

    @Override
    public int getFixedTimestep(Map<String, Object> parameters) {
        return fixedTimestep;
    }

    @Override
    public void build(PolysunSettings polysunSettings, Map<String, Object> parameters) throws PluginControllerException {
        super.build(polysunSettings, parameters);
        fixedTimestep = getProperty(PROPERTY_FIXED_TIMESTEP).getInt();
        verboseLevel = getProperty(PROPERTY_VERBOSE_LEVEL).getInt();
        matlabPropertiesFloat = new float[propertiesFloat.length - numInternProperties];
        System.arraycopy(propertiesFloat, numInternProperties, matlabPropertiesFloat, 0, propertiesFloat.length - numInternProperties);
        matlabPropertiesString = new String[propertiesString.length - numInternProperties];
        System.arraycopy(propertiesString, numInternProperties, matlabPropertiesString, 0, propertiesString.length - numInternProperties);
    }
}
