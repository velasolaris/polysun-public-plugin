/**
 * Package containing the MatlabPluginController using the MatConsoleCtl library.
 *
 * @see com.velasolaris.plugin.controller.matlab.matconsolectl.MatlabPluginController
 */
package com.velasolaris.plugin.controller.matlab.matconsolectl;
