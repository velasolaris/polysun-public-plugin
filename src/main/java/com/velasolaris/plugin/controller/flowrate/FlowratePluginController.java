package com.velasolaris.plugin.controller.flowrate;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import com.velasolaris.plugin.controller.spi.AbstractPluginController;
import com.velasolaris.plugin.controller.spi.PluginControllerConfiguration;
import com.velasolaris.plugin.controller.spi.PluginControllerConfiguration.ControlSignal;
import com.velasolaris.plugin.controller.spi.PluginControllerConfiguration.Property;
import com.velasolaris.plugin.controller.spi.PluginControllerConfiguration.Sensor;
import com.velasolaris.plugin.controller.spi.PolysunSettings;
import com.velasolaris.plugin.controller.spi.PolysunSettings.PropertyValue;

/**
 * Controls the status (on/off) of one or two different components and the
 * flowrate of a given pump based on one or two flowrate sensors.
 *
 * The FlowratePluginController provides the same functionality as the
 * FlowrateController.
 *
 * The DemoFlowratePluginController is a copy of this class.
 * The FlowratePluginController implementation is written in name based approach.
 * The DemoFlowratePluginController implementation is written in an array access style.
 *
 * The names of properties, sensors or control signals in this class are internal Polysun translation keys.
 * Normal text can also be used.
 *
 * See also ch.spf.PS4Controller.FlowrateController.
 *
 * @see com.velasolaris.plugin.controller.demo.DemoFlowratePluginController
 *
 * @author rkurmann
 * @since Polysun 9.1
 *
 */
public class FlowratePluginController extends AbstractPluginController {

    /** Static instance of the Logger for this class */
    protected static Logger sLog = Logger.getLogger(FlowratePluginController.class.getName());

    private static final int DESGIN_FLOWRATE_VARIABLE = 0;
    private static final int DESGIN_FLOWRATE_FIXED = 1;

    /**
     * Default constructor.
     */
    public FlowratePluginController() {
    }

    @Override
    public String getName() {
        return "Flowrate";
    }

    @Override
    public PluginControllerConfiguration getConfiguration(Map<String, Object> parameters) {
        List<Property> properties = new ArrayList<>();
        // Definition flow rate setting: Variable value / Fixed value
        properties.add(new Property("Flowrate type", new String[] { "Variable flowrate", "Fixed flowrate" }, 1));
        properties.add(new Property("Fixed flowrate", 10f, 0, 100000, "l/h")); // Fixed flow rate
        properties.add(new Property("Flowrate scale", 1, 0.1f, 1000, "l/h")); // Scaling factor

        List<Sensor> sensors = new ArrayList<>();
        sensors.add(new Sensor("Flowrate sensor 1", "l/h", true, true)); // Flow rate sensor 1
        sensors.add(new Sensor("Flowrate sensor 2", "l/h", true, false)); // Flow rate sensor 2
        sensors.add(new Sensor("Variable flowrate sensor", "l/h", true, false)); // Variable flow rate

        List<ControlSignal> controlSignals = new ArrayList<>();
        controlSignals.add(new ControlSignal("Signal pump 1", "", false, true)); // On/Off pump 1
        controlSignals.add(new ControlSignal("Signal pump 2", "", false, false)); // On/Off pump 2
        controlSignals.add(new ControlSignal("Flowrate pump 1", "l/h", true, false)); // Pumping capacity pump 1

        return new PluginControllerConfiguration(properties, sensors, controlSignals, null, 0, 0, 0, "plugin/images/controller_plugin.png",
                null);
    }

    @Override
    public int[] control(int simulationTime, boolean status, float[] sensors, float[] controlSignals, float[] logValues,
            boolean preRun, Map<String, Object> parameters) {
        float statusSignal = 0;
        float measuredFlowrate = getSensor("Flowrate sensor 1", sensors)
            + (getSensor("Flowrate sensor 2").isUsed() ? getSensor("Flowrate sensor 2", sensors) : 0);
        if (status && getProp("Flowrate type").getInt() == DESGIN_FLOWRATE_VARIABLE
                && getSensor("Variable flowrate sensor").isUsed()) {
            // Variable flowrate case:
            // If the measured flow rate(s) is/are bigger then the variable
            // flowrate, set status to 1
            statusSignal = measuredFlowrate > getSensor("Variable flowrate sensor", sensors) ? 1 : 0;
        } else if (status && getProp("Flowrate type").getInt() == DESGIN_FLOWRATE_FIXED) {
            // Fixed flowrate case:
            // If the measured flow rate(s) is/are bigger then the fixed
            // flowrate, set status to 1
            statusSignal = measuredFlowrate > getProp("Fixed flowrate").getFloat() ? 1 : 0;
        }
        controlSignals[getCSIdx("Signal pump 1")] = controlSignals[getCSIdx("Signal pump 2")] = statusSignal;

        if (getControlSignal("Flowrate pump 1").isUsed()) {
            // Measured flowrate is scaled to the output, if used
            controlSignals[getCSIdx("Flowrate pump 1")] = measuredFlowrate * getProp("Flowrate scale").getFloat();
        }
        return null;
    }

    @Override
    public List<String> getPropertiesToHide(PolysunSettings propertyValues, Map<String, Object> parameters) {
        List<String> propertiesToHide = new ArrayList<>();

        // Show 'Fixed flowrate' only if 'Flowrate type' = 'Fixed'
        PropertyValue property = propertyValues.getPropertyValue("Flowrate type");
        if (property != null && property.getInt() != DESGIN_FLOWRATE_FIXED) {
            propertiesToHide.add("Fixed flowrate");
        }
        return propertiesToHide;
    }

    @Override
    public List<String> getSensorsToHide(PolysunSettings propertyValues, Map<String, Object> parameters) {
        List<String> propertiesToHide = new ArrayList<>();
        // Show sensor 'Variable flowrate sensor' show if 'Flowrate type' = 'Variable'
        PropertyValue property = propertyValues.getPropertyValue("Flowrate type");
        if (property != null && property.getInt() != DESGIN_FLOWRATE_VARIABLE) {
            propertiesToHide.add("Variable flowrate sensor");
        }
        return propertiesToHide;
    }

    @Override
    public List<String> getControlSignalsToHide(PolysunSettings aPropertyValues, Map<String, Object> aParameters) {
        return null;
    }

    @Override
    public String getDescription() {
        // Here we return an internal Polysun translation key
        // Otherwise return "Controls the status (on/off) of one or two
        // different components and the flowrate of a given pump based on two
        // flowrate sensors"
        return "2_analoge_Eingaenge,_2_digitale_Ausgaenge,_1_analoger_Ausgang";
    }

}
