/**
 * Package containing the FlowratePluginController.
 *
 * @see com.velasolaris.plugin.controller.flowrate.FlowratePluginController
 */
package com.velasolaris.plugin.controller.flowrate;
