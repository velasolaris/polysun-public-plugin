package com.velasolaris.plugin.controller.publicplugin;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.velasolaris.plugin.controller.flowrate.FlowratePluginController;
import com.velasolaris.plugin.controller.matlab.matconsolectl.MatlabPluginController;
import com.velasolaris.plugin.controller.matlab.matconsolectl.MatlabRuntimePluginController;
import com.velasolaris.plugin.controller.rpc.SimpleRpcPluginController;
import com.velasolaris.plugin.controller.spi.AbstractControllerPlugin;
import com.velasolaris.plugin.controller.spi.IPluginController;

/**
 * Controller plugin for open source plugin controllers.
 *
 * @author rkurmann
 * @since Polysun 9.1
 *
 */
public class PublicControllersPlugin extends AbstractControllerPlugin {

    @Override
    public List<Class<? extends IPluginController>> getControllers(Map<String, Object> aParameters) {
        List<Class<? extends IPluginController>> controllers = new ArrayList<>();
        controllers.add(FlowratePluginController.class);
        controllers.add(MatlabPluginController.class);
        controllers.add(MatlabRuntimePluginController.class);
        controllers.add(SimpleRpcPluginController.class);
        return controllers;
    }

    @Override
    public String getDescription() {
        return "Open source plugin controllers for Polysun";
    }

    @Override
    public String getCreator() {
        return "Vela Solaris AG";
    };

    @Override
    public String getVersion() {
        return "9.1";
    }


}
