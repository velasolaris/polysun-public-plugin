package com.velasolaris.plugin.util;

import org.apache.commons.lang3.exception.ExceptionUtils;

/**
 * Collection of utility methods.
 * @author Marc Jakobi (MAJ)
 * @since Polysun 11.3
 */
public class PluginUtils {

    /**
     * Builds a {@code String} from the {@code String[]} returned by {@code ExceptionUtils.getRootCauseStrackTrace()}.
     * @param t the throwable to examine, may be null
     * @return the stack trace frames
     * @see org.apache.commons.lang3.ExceptionUtils
     */
    public static String getRootCauseStackTrace(Throwable t) {
        StringBuffer rootStackTrace = new StringBuffer();
        for (String s : ExceptionUtils.getRootCauseStackTrace(t)) {
            rootStackTrace.append(s);
            rootStackTrace.append("/n");
        }
        String rootCauseStackTrace = rootStackTrace.toString();
        return rootCauseStackTrace;
    }
}
