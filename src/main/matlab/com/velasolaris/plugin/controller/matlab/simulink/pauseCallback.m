% pauseCallback.m: Callback function called in before paused state
% Model Callback Parameter: PauseFcn
% https://ch.mathworks.com/help/simulink/ug/model-callbacks.html

global CO_SIM_EXCHANGE;

if CO_SIM_EXCHANGE.verboseLevel > 1
    t = get_param(bdroot, 'SimulationTime');
	% disp() does not work in callbacks
    fprintf(2, ['Simulink PauseCallback t=', num2str(t), '\n']);
end
