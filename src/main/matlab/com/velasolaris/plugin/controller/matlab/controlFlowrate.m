function [ controlSignals, logValues, timepoints ] = controlFlowrate( simulationTime, status, sensors, sensorsUsed, properties, propertiesStr, preRun, controlSignalsUsed, numLogValues, stage, fixedTimestep, verboseLevel, parameters )
%controlFlowrate Controls the status (on/off) of one or two different components and the flowrate of a given pump based on two flowrate sensors
%
% This function provides the same functionality as the normal flowrate controller of Polysun.
% The flowrate controller in Polysun could be replaced by a MatlabPluginController calling this function.
% E.g. See template 16c in Standard Polysun templates.
%
% Properties:
% 1: Flowrate type [0 = variable / 1 = fixed]
% 2: Fixed flowrate, [l/h]
% 3: Flowrate scale, [0.1 .. 1000]
%
% Sensors:
% 1: Flowrate sensor 1 [l/h]
% 2: Flowrate sensor 2 [l/h]
% 3: Variable flowrate sensor [l/h]
%
% Control signals:
% 1: Signal pump 1 [0/1]
% 2: Flowrate pump 1 [l/h]
%
% Parameters are described in control.m

%% Simulation management
% Initialization part and simulation progress outputs

persistent lastDay;

if stage == 0
    disp 'Init simulation';
    lastDay = 0;
    controlSignals = [];
    logValues = [];
    timepoints = [];
    return;
elseif stage == 2
    disp 'Terminate simulation';
    controlSignals = [];
    logValues = [];
    timepoints = [];
    return;
end

day = fix(simulationTime / (3600 * 24));
if (~preRun && verboseLevel >= 2 && day > lastDay)
    disp(['Day ', num2str(fix(day))]);
    lastDay = day;
elseif (~preRun && verboseLevel == 1 && day > lastDay + 7)
    disp(['Week ', num2str(fix(day / 7))]);
    lastDay = day;
elseif (~preRun && day > lastDay + 30)
    disp(['Month ', num2str(fix(day / 30))]);
    lastDay = day;
end

%% Control signal calculation
% Here are the control signals calculated and returned

controlSignals = zeros(1, length(controlSignalsUsed));
statusSignal = 0;
measuredFlowrate = sensors(1);
if sensorsUsed(2)
    measuredFlowrate = measuredFlowrate + sensors(2);
end
if status && properties(1) == 0 && sensorsUsed(3)
    % Variable flowrate case:
    % If the measured flow rate(s) is/are bigger then the variable
    % flowrate, set status to 1
    statusSignal = measuredFlowrate > sensors(3);
elseif status && properties(1) == 1
    % Fixed flowrate case:
    % If the measured flow rate(s) is/are bigger then the fixed
    % flowrate, set status to 1
    statusSignal = measuredFlowrate > properties(2);
end
controlSignals(1) = statusSignal;

if controlSignalsUsed(2)
    % Measured flowrate is scaled to the output, if used
    controlSignals(2) = measuredFlowrate * properties(3);
end

logValues = zeros(1, numLogValues);
timepoints = [];

end
