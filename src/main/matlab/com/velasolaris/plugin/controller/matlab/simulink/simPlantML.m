function [ SensorSig, fIniMatlab ] = simPlantML( SLMdl, fIniMatlab, CtrlSig )
% Simple plant model (room model) to emulate Polysun
% DESCRIPTION
%   TR is increased at a constant slope when CtrlSig>0, and decreased at a smaller slope otherwise.

% Declaration and initialization of persistent variables
persistent TR_Matlab;
persistent SimuTime_old;
if fIniMatlab,
    TR_Matlab = 17; % [°C]
    SimuTime_old = 0; % [sec]
    dumA = 3; % for setting a breakpoint
    fIniMatlab = 0; % Reset fIniMatlab when initialization has been done
end

simuTime = get_param( SLMdl, 'SimulationTime' );
dSimuTime = simuTime - SimuTime_old;
% disp(['simuTime = ', num2str(simuTime)]);
% disp(['dSimuTime = ', num2str(dSimuTime)]);

%%
if CtrlSig > 0
    TR_Matlab = TR_Matlab + 0.2*(dSimuTime/60);  % Not an integer multiple of (below)
else
    TR_Matlab = TR_Matlab - 0.094*(dSimuTime/60); % Not an integer multiple of (above)
end
SensorSig = TR_Matlab;

%% Update
SimuTime_old = simuTime;

end

