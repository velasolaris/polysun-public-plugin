% SimulinkBreak.m: Causes the simulation to pause at the desired time.

global CO_SIM_EXCHANGE;

% Note: There is no guarantee from Simulinkt that the pause is at the same time as this assert time
set_param(bdroot,'SimulationCommand','pause');

if CO_SIM_EXCHANGE.verboseLevel > 1
    t = get_param(bdroot, 'SimulationTime');
    disp(['Simulink Assert t=', num2str(t)])
end
