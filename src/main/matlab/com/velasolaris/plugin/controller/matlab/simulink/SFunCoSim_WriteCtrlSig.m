function SFunCoSim_WriteCtrlSig(block)
% Level-2 MATLAB file S-Function for times two demo.
%   Copyright 1990-2009 The MathWorks, Inc.

  setup(block);

%endfunction

function setup(block)
  %% Register number of input and output ports
  block.NumInputPorts  = 1;
  block.NumOutputPorts = 0;

  %% Register the parameters.
  block.NumDialogPrms     = 1;
  block.DialogPrmsTunable = {'Nontunable'}; % Options include: 'Tunable','Nontunable','SimOnlyTunable'};

  numOfCtrSig = block.DialogPrm(1).Data;

  %% Setup functional port properties to dynamically inherited.
  block.SetPreCompInpPortInfoToDynamic;
  block.SetPreCompOutPortInfoToDynamic;

  block.InputPort(1).DirectFeedthrough = true; %false;
  block.InputPort(1).Dimensions = numOfCtrSig;


  %% Set block sample time to inherited
  block.SampleTimes = [-1 0];

  %% Set the block simStateCompliance to default (i.e., same as a built-in block)
  block.SimStateCompliance = 'DefaultSimState';

  %% Register methods
  block.RegBlockMethod('Outputs',                 @Output);

%endfunction

function Output(block)
  global CO_SIM_EXCHANGE;
  % Control signals (Simulink --> Polysun)
  CtrlSigIn = block.InputPort(1).Data;

  % disp(['controlled s-function in=', num2str(in)]);

  CO_SIM_EXCHANGE.CurrentControlSignals = CtrlSigIn;

  if CO_SIM_EXCHANGE.verboseLevel > 1
    disp(['SFunWriteCtrl: ', num2str(CtrlSigIn(1)), '   t=', num2str(get_param(bdroot,'SimulationTime'))]);
  end

%endfunction
