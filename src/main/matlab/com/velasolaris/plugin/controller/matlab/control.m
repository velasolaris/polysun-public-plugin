function [ controlSignals, logValues, timepoints ] = control( simulationTime, status, sensors, sensorsUsed, properties, propertiesStr, preRun, controlSignalsUsed, numLogValues, stage, fixedTimestep, verboseLevel, parameters )
%control Control function template for Polysun MatlabPluginController
%
% Does the control work. This method is called each timestep by Polysun if signals must be controlled
% according to the sensor values.
%
% Input arguments:
% simulationTime, int: The simulation time in [s] beginning from the 1. January 00:00 (no leap year).
% status, 0/1: The status of this controller according to user settings,
% 		1 means enabled,
% 		0 disabled.
% 		The status originates from the timer setting of the controller dialog. The user can enable
% 		or disable the controller for certain hours, days or month.
% 		This value should be respected by the controller implementation, otherwise it
% 		could lead to an unexpected user experience.
% sensors, float vector: The values of the sensors configured by the user (Input parameter)
%		(length is available during init stage = 0)
% sensorsUsed, vector 0/1: 1 indicates that the sensor is used in Polysun (available during init stage = 0)
% properties, float vector: The properties set in Polysun (available during init stage = 0)
% propertiesStr, String vector: The properties set in Polysun as string array (available during init stage = 0)
% preRun, 0/1: Is this the real simulation or a pre run phase? This value can be ignored.
% controlSignalsUsed, float vector: 1 indicates that the control signal is used in Polysun (available during init stage = 0)
% numLogValues, int: The number of log values that can be returned. Configurable in Polysun.
% stage, int: Stage of the function call
% 		0 = Init, called before the simulation to init (initSimulation), results will be ignored
%		1 = during the simulation (simulation),
%		2 = after the simulation (terminateSimulation), results will be ignored
% fixedTimestep, int: Fixed timestep. For each timepoint which is a multiple of this fixedTimestep, the simulation does
%		a timestep. The Polysun solver can do more timesteps if necessary. Example, for fixedTimestep of 180s, the
%		simulation solver does a simulation at least at 0s, 180s, 360s, 480s, 720s, ...
%		0 means no fixed timestep and Polysun uses the default timesteps (240s during the day and 720s during the
%		night).
% verboseLevel, int: How much output should this function display to the console? (available during init stage = 0)
%		0 = standard output
%		1 = verbose output
%		2 = debug output
% parameters, Map<String, Object>: Generic parameters
%
% Output arguments:
% controlSignals: The control signals set by this plugin controller (Output parameter).
% logValues: The values to log in Polysun, e.g intermediate results. This value can be ignored.
%       These values are shown in the Simulation Analysis or in the Log & Parameterizing output.
% timepoints [s].
% 		Registers these timepoints in the future, where the simulation have to do a timestep.
% 		It doesn't matter, if the same timepoint will be registered several times.
% 		Timepoint in the array is in seconds from the 1. Jan. 00:00, or <code>null</code> if no additional timesteps are required.
% 		These timesteps can be used for time based controlling strategies.
%
% Internally, Polysun calculates with floats. The arguments passed to and received from Matlab will be converted to floats.

%% Simulation management
% Initialization part and simulation progress outputs

persistent lastDay;

if stage == 0
    disp 'Init simulation';
    lastDay = 0;
    controlSignals = [];
    logValues = [];
    timepoints = [];
    return;
elseif stage == 2
    disp 'Terminate simulation';
    controlSignals = [];
    logValues = [];
    timepoints = [];
    return;
end

day = fix(simulationTime / (3600 * 24));
if (~preRun && verboseLevel >= 2 && day > lastDay)
    disp(['Day ', num2str(fix(day))]);
    lastDay = day;
elseif (~preRun && verboseLevel == 1 && day > lastDay + 7)
    disp(['Week ', num2str(fix(day / 7))]);
    lastDay = day;
elseif (~preRun && day > lastDay + 30)
    disp(['Month ', num2str(fix(day / 30))]);
    lastDay = day;
end

%% Control signal calculation
% Here are the control signals calculated and returned

    % disp(status);
    % disp(sensors);
    % disp(sensorsUsed);
    controlSignals = zeros(1, length(controlSignalsUsed));
    logValues = zeros(1, numLogValues);
    timepoints = [];
end
