function SFunCoSim_ReadSensors(block)
% Level-2 MATLAB file S-Function for times two demo.
%   Copyright 1990-2009 The MathWorks, Inc.

  setup(block);

%endfunction

function setup(block)
  %% Register number of input and output ports
  block.NumInputPorts  = 1;
  block.NumOutputPorts = 1;

  %% Register the parameters.
  block.NumDialogPrms     = 1;
  block.DialogPrmsTunable = {'Nontunable'}; % Options iclude: 'Tunable','Nontunable','SimOnlyTunable'};

  numOfSensorSig = block.DialogPrm(1).Data;

  %% Setup functional port properties to dynamically inherited.
  block.SetPreCompInpPortInfoToDynamic;
  block.SetPreCompOutPortInfoToDynamic;

  block.InputPort(1).DirectFeedthrough = true; %false;
  block.InputPort(1).Dimensions = 1;

  block.OutputPort(1).Dimensions = numOfSensorSig;

  %% Set block sample time to inherited
  block.SampleTimes = [-1 0];

  %% Set the block simStateCompliance to default (i.e., same as a built-in block)
  block.SimStateCompliance = 'DefaultSimState';

  %% Register methods
  block.RegBlockMethod('Outputs',                 @Output);

%endfunction

function Output(block)
global CO_SIM_EXCHANGE;
  bReadSensorSignals = block.InputPort(1).Data;

% Sensor signals (Polysun --> Simulink)
  for i=1:block.OutputPort(1).Dimensions  % length(block.OutputPort(1).Data)
      block.OutputPort(1).Data(i) = CO_SIM_EXCHANGE.Sensors(i);
  end

  if CO_SIM_EXCHANGE.verboseLevel > 1
    disp(['SFunReadSensor: ', num2str(CO_SIM_EXCHANGE.Sensors(1)), '   t=', num2str(get_param(bdroot,'SimulationTime'))]);
  end

  % FIXME The for-loop above should be replaced by:
  % block.OutputPort(1).Data = CO_SIM_EXCHANGE.Sensors;
%endfunction
