function [ controlSignals, logValues, timepoints ] = controlCoSimSimulink( simulationTimePS, status, sensors, sensorsUsed, properties, propertiesStr, preRun, controlSignalsUsed, numLogValues, stage, fixedTimestep, verboseLevel, parameters)
%controlCoSimSimulink Co-simulation function Polysun/Simulink
%
% Parameters are described in control.m

% Note: When you use set_param to start, pause, continue or stop a simulation,
% the commands are requests for such actions and the simulation doesn't execute them immediately.
% Simulink® first completes uninterruptable work, such as solver steps and other commands that preceded the
% set_param command.
% https://ch.mathworks.com/help/simulink/ug/control-simulations-programmatically.html

% Model Callback Parameter: PauseFcn
% https://ch.mathworks.com/help/simulink/ug/model-callbacks.html

% TODO write loop script to emulate Polysun for independent analysis

%% Simulation management
% Initialization part and simulation progress outputs

% General declarations
global CO_SIM_EXCHANGE;
SLMdl = 'SimulinkModel'; % Name of Simulink model, must be in path of Matlab

persistent lastControlSignals;
persistent lastSec;
persistent ParCosim;
persistent DebugStruct;
persistent errorCounter;

errorThreshold = 10;

if stage == 0
    disp 'Init simulation';

    if verboseLevel > 1
        DebugStruct = struct('SimuTimePS', [], 'SimuTimeSL', [], 'Sensor1ML', [], 'CtrlSig1ML', []);
    end

    lastSec = 0;
    errorCounter = 0;
    set_param( SLMdl, 'SimulationCommand', 'stop' );
    if verboseLevel > 1
       set_param(SLMdl,'PauseFcn','pauseCallback');
    end
    % Parameterization of Cosimulation interface
    % The fixed timestep of Polysun for exchange
    ParCosim.TiS = fixedTimestep;
    % The number of sensors configured in Polysun
    ParCosim.numOfSensorSig = sum(sensorsUsed);
    % sensor(1): T tank layer 4 [°C]

    % The number of controls signals configured in Polysun
    ParCosim.numOfCtrSig = sum(controlSignalsUsed);
    % We have to catch the first assertion at t = -1
    ParCosim.startTime = -1.1;
    ParCosim.stopTime = 3600 * 24 * 30;
    % controlSignal(1) = Signal to internal heater of tank, 1 = enabled
    % controlSignal(2) = Power to heat for the internal heater of tank
    assignin('base', 'ParCosim', ParCosim); % write to base workspace and thus available in Simulink

    lastControlSignals = zeros(1, ParCosim.numOfCtrSig);

    % Parameterization of plant model
    Bldg.SpecificHeatCns = 333; % [W/K]. Required heat of GebaeudeSimpel is (TRSp-TA) * Bldg.SpecificHeatCns
        % Used for parameterisation of bulding, and for initialization of controller.
    assignin('base', 'Bldg', Bldg); % write to base workspace and thus available in Simulink

    % Initialization of data exchange struct
    CO_SIM_EXCHANGE = struct( 'Sensors', zeros(1,ParCosim.numOfSensorSig), ...
        'ControlSignals', zeros(1,ParCosim.numOfCtrSig), ...
        'CurrentControlSignals', zeros(1,ParCosim.numOfSensorSig), 'verboseLevel', verboseLevel );

    % Open Simulink model unless it is already loaded
    % Seems to work reliable...
    if ~bdIsLoaded(SLMdl)
        open_system(SLMdl);
    end

    set_param( SLMdl, 'StartTime', num2str(ParCosim.startTime) );
    set_param( SLMdl, 'StopTime', num2str(ParCosim.stopTime) );

    controlSignals = [];
    logValues = [];
    timepoints = [];
    return;
elseif stage == 2
    disp 'Terminate simulation';
    controlSignals = [];
    logValues = [];
    timepoints = [];
    return;
end

if (~preRun && verboseLevel > 1 && simulationTimePS > lastSec + 60)
    disp(['Min ', num2str(fix(simulationTimePS / 60))]);
    lastSec = simulationTimePS;
elseif (~preRun && verboseLevel == 1 && simulationTimePS > lastSec + 3600)
    disp(['Hour ', num2str(fix(simulationTimePS / 3600))]);
    lastSec = simulationTimePS;
elseif (~preRun && simulationTimePS > lastSec + 3600 * 24)
    disp(['Day ', num2str(fix(simulationTimePS / 3600 / 24))]);
    lastSec = simulationTimePS;
end

% If we are not in a fixed exchange timepoint, return last control signals without calling Simulink
if mod(simulationTimePS, fixedTimestep) ~= 0
    controlSignals = lastControlSignals;
    logValues = zeros(1, numLogValues);
    timepoints = [];
    return;
elseif fixedTimestep == 0
    error('The fixedTimestep must not be zero in Polysun');
end

if verboseLevel > 1
    disp(['SimuTimePS ', num2str(simulationTimePS)]);
end

%% Control signal calculation
% Here are the control signals calculated and returned

% Continue to next timestep, wait is at the beginning of the next function
% For optimization, command below can be commented out and commented in at the end,
% then Simulink and Polysun can simulate in parallel
set_param( SLMdl, 'SimulationCommand', 'continue' );

% Wait until Matlab is in paused state
state = waitPaused( SLMdl );
if ~strcmp(state, 'paused') && simulationTimePS ~= 0
    disp(['WRONG STATE: ', state]);
    errorCounter = errorCounter + 1;
    if errorCounter > errorThreshold
        error(['Too many times in wrong state: ', errorCounter]);
    end
end

% Set new sensor signals from Polysun for Simulink
for iSensor = 1:ParCosim.numOfSensorSig
    CO_SIM_EXCHANGE.Sensors(iSensor) = sensors(iSensor);
end

%% Start Simulink simulation, we have to synchronize Polysun and Simulink
if simulationTimePS == 0
    disp(['Start Simulink']);
    set_param( SLMdl, 'SimulationCommand', 'start' );
    % Wait until Matlab is in paused state
    state = waitPaused( SLMdl );
    if ~strcmp(state, 'paused')
        % This might be a problem, except before start, since we start below
        disp(['WRONG STATE: ', state]);
        error('Wrong state after Simulink simulation start');
    end
end

simulationTimeSL = get_param(SLMdl,'SimulationTime');

if verboseLevel > 1
    DebugStruct.SimuTimePS = [DebugStruct.SimuTimePS; simulationTimePS];
    DebugStruct.SimuTimeSL = [DebugStruct.SimuTimeSL; simulationTimeSL];
    DebugStruct.Sensor1ML = [DebugStruct.Sensor1ML; CO_SIM_EXCHANGE.Sensors(1)];
    DebugStruct.CtrlSig1ML = [DebugStruct.CtrlSig1ML; CO_SIM_EXCHANGE.CurrentControlSignals(1)];
    DebugVec = [simulationTimePS, get_param(SLMdl,'SimulationTime')  CO_SIM_EXCHANGE.Sensors(1)  CO_SIM_EXCHANGE.CurrentControlSignals(1)];
    fprintf('  controlVecReadSensor:         '); fprintf(' %6.2f', DebugVec(:));  fprintf('  \n');
    assignin('base', 'DebugStruct', DebugStruct);
end

if simulationTimeSL > simulationTimePS
    disp(['Warning, simulationTimeSL bigger than simulationTimePS tSL=', num2str(simulationTimeSL, '%f'), ' tPS=', num2str(simulationTimeSL, '%f')]);
end

% Continue to t + 2s
set_param( SLMdl, 'SimulationCommand', 'continue' );
waitPaused( SLMdl );

simulationTimeSL = get_param(SLMdl,'SimulationTime');

if verboseLevel > 1
    DebugStruct.SimuTimePS = [DebugStruct.SimuTimePS; simulationTimePS];
    DebugStruct.SimuTimeSL = [DebugStruct.SimuTimeSL; simulationTimeSL];
    DebugStruct.Sensor1ML = [DebugStruct.Sensor1ML; CO_SIM_EXCHANGE.Sensors(1)];
    DebugStruct.CtrlSig1ML = [DebugStruct.CtrlSig1ML; CO_SIM_EXCHANGE.CurrentControlSignals(1)];
    DebugVec = [simulationTimePS, get_param(SLMdl,'SimulationTime')  CO_SIM_EXCHANGE.Sensors(1)  CO_SIM_EXCHANGE.CurrentControlSignals(1)];
    fprintf('  controlVecWriteCtrl:          '); fprintf(' %6.2f', DebugVec(:));  fprintf('  \n');
    assignin('base', 'DebugStruct', DebugStruct);
end

% Read control signals from Simulink and set as return value for Polysun
% controlSignals = zeros(1, length(controlSignalsUsed));
CO_SIM_EXCHANGE.ControlSignals = CO_SIM_EXCHANGE.CurrentControlSignals;  % TODO Is this necessary?
lastControlSignals = CO_SIM_EXCHANGE.ControlSignals;
% controlSignals = CO_SIM_EXCHANGE.ControlSignals;
% Set new sensor signals from Polysun for Simulink
controlSignals = zeros(1, ParCosim.numOfCtrSig);
for iCtrSig = 1:ParCosim.numOfCtrSig
    controlSignals(iCtrSig) = CO_SIM_EXCHANGE.ControlSignals(iCtrSig);
end

% % Continue to next timestep, wait is at the beginning of the next function
% % Calling continue here, is a performance optimization
% set_param( SLMdl, 'SimulationCommand', 'continue' );

logValues = zeros(1, numLogValues);
timepoints = [];
end

function state = waitPaused( SLMdl )
    while strcmp(get_param( SLMdl, 'SimulationStatus'), 'running')
        pause(0.1)
    end
    state = get_param( SLMdl, 'SimulationStatus');
end
