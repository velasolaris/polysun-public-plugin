"""
Polysun plugin controller functions for SimpleRpcPluginController.

The function control() describes the interface.
The function control_flowrate() is a flowrate controller, see Example in Polysun.

The Polysun user manual describes the usage of plugin controllers.

"""

from __future__ import division, unicode_literals, print_function, absolute_import, with_statement  # Ensure compatibility with Python 3
from utils import static_vars

__author__ = 'Roland Kurmann'
__email__ = 'roland dot kurmann at velasolaris dot com'
__url__ = 'http://velasolaris.com'
__license__ = 'MIT'
__version__ = '9.1'

def control(simulationTime, status, sensors, sensorsUsed, properties, propertiesStr, preRun, controlSignalsUsed, numLogValues, stage, fixedTimestep, verboseLevel, parameters):
    """control(simulationTime, status, sensors, sensorsUsed, properties, propertiesStr, preRun, controlSignalsUsed, numLogValues, stage, fixedTimestep, verboseLevel, parameters) => controlSignals, logValues, timepoints

    Control function template for Polysun Python controllers.

     Always a result has to be returned, None is not supported in RPC, best return [] for empty results.

    Does the control work. This method is called each timestep by Polysun if signals must be controlled
    according to the sensor values.

    Input arguments:
    simulationTime, int: The simulation time in [s] beginning from the 1. January 00:00 (no leap year).
    status, 0/1: The status of this controller according to user settings,
            1 means enabled,
            0 disabled.
            The status originates from the timer setting of the controller dialog. The user can enable
            or disable the controller for certain hours, days or month.
            This value should be respected by the controller implementation, otherwise it
            could lead to an unexpected user experience.
    sensors, float vector: The values of the sensors configured by the user (Input parameter)
           (length is available during init stage = 0)
    sensorsUsed, vector 0/1: 1 indicates that the sensor is used in Polysun (available during init stage = 0)
    properties, float vector: The properties set in Polysun (available during init stage = 0)
    propertiesStr, String vector: The properties set in Polysun as string array (available during init stage = 0)
    preRun, 0/1: Is this the real simulation or a pre run phase? This value can be ignored.
    controlSignalsUsed, float vector: 1 indicates that the control signal is used in Polysun (available during init stage = 0)
    numLogValues, int: The number of log values that can be returned. Configurable in Polysun.
    stage, int: Stage of the function call
            0 = Init, called before the simulation to init (initSimulation), results will be ignored
           1 = during the simulation (simulation),
           2 = after the simulation (terminateSimulation), results will be ignored
    fixedTimestep, int: Fixed timestep. For each timepoint which is a multiple of this fixedTimestep, the simulation does
           a timestep. The Polysun solver can do more timesteps if necessary. Example, for fixedTimestep of 180s, the
           simulation solver does a simulation at least at 0s, 180s, 360s, 480s, 720s, ...
           0 means no fixed timestep and Polysun uses the default timesteps (240s during the day and 720s during the
           night).
    verboseLevel, int: How much output should this function display to the console? (available during init stage = 0)
           0 = standard output
           1 = verbose output
           2 = debug output
    parameters, Map<String, Object>: Generic parameters

    Output arguments:
    controlSignals, float vector: The control signals set by this plugin controller (Output parameter).
    logValues, float vector: The values to log in Polysun, e.g intermediate results. This value can be ignored.
          These values are shown in the Simulation Analysis or in the Log & Parameterizing output.
    timepoints, int vector [s].
            Registers these timepoints in the future, where the simulation have to do a timestep.
            It doesn't matter, if the same timepoint will be registered several times.
            Timepoint in the array is in seconds from the 1. Jan. 00:00, or <code>null</code> if no additional timesteps are required.
            These timesteps can be used for time based controlling strategies.

    Internally, Polysun calculates with floats. The arguments passed to and received from Python will be converted to floats.

    """
    controlSignals = [0] * len(controlSignalsUsed)
    logValues = [0] * numLogValues
    timepoints = []
    return controlSignals, logValues, timepoints

@static_vars(lastDay=0)  # static_vars work only for a single thread
def control_flowrate(simulationTime, status, sensors, sensorsUsed, properties, propertiesStr, preRun, controlSignalsUsed, numLogValues, stage, fixedTimestep, verboseLevel, parameters):
    """Controls the status (on/off) of one or two different components and the flowrate of a given pump based on two flowrate sensors

    control(simulationTime, status, sensors, sensorsUsed, properties, propertiesStr, preRun, controlSignalsUsed, numLogValues, stage, fixedTimestep, verboseLevel, parameters) => controlSignals, logValues, timepoints

    This function provides the same functionality as the normal flowrate controller of Polysun.
    The flowrate controller in Polysun could be replaced by a MatlabPluginController calling this function.
    E.g. See template 16c in Standard Polysun templates.

    Properties:
    1: Flowrate type [0 = variable / 1 = fixed]
    2: Fixed flowrate, [l/h]
    3: Flowrate scale, [0.1 .. 1000]

    Sensors:
    1: Flowrate sensor 1 [l/h]
    2: Flowrate sensor 2 [l/h]
    3: Variable flowrate sensor [l/h]

    Control signals:
    1: Signal pump 1 [0/1]
    2: Flowrate pump 1 [l/h]

    Parameters are described in control()
    """

    # Simulation management
    # Initialization part and simulation progress outputs

    if stage == 0:
        print('Init simulation')
        control_flowrate.lastDay = 0
        return []
    elif stage == 2:
        print('Terminate simulation')
        return []

    day = simulationTime // (3600 * 24)
    if not preRun and verboseLevel >= 2 and day > control_flowrate.lastDay:
        print('Day ' + str(day))
        control_flowrate.lastDay = day
    elif not preRun and verboseLevel == 1 and day > control_flowrate.lastDay + 7:
        print('Week ' + str(day // 7))
        control_flowrate.lastDay = day
    elif not preRun and day > control_flowrate.lastDay + 30:
        print('Month ' + str(day // 30))
        control_flowrate.lastDay = day

    # Control signal calculation
    # Here are the control signals calculated and returned

    controlSignals = [0] * len(controlSignalsUsed)
    statusSignal = 0
    measuredFlowrate = sensors[0]
    if sensorsUsed[1]:
        measuredFlowrate = measuredFlowrate + sensors[1]

    if status and properties[0] == 0 and sensorsUsed[2]:
        # Variable flowrate case:
        # If the measured flow rate(s) is/are bigger then the variable
        # flowrate, set status to 1
        statusSignal = measuredFlowrate > sensors[2]
    elif status and properties[0] == 1:
        # Fixed flowrate case:
        # If the measured flow rate(s) is/are bigger then the fixed
        # flowrate, set status to 1
        statusSignal = measuredFlowrate > properties[1]

    controlSignals[0] = statusSignal

    if controlSignalsUsed[1]:
        # Measured flowrate is scaled to the output, if used
        controlSignals[1] = measuredFlowrate * properties[2]

    logValues = [0] * numLogValues
    timepoints = []

    return controlSignals, logValues, timepoints
