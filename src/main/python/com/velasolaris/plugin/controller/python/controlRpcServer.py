#!/usr/bin/env python
"""
RPC Server starter for Polysun plugin controller functions of SimpleRpcPluginController.

Functions from controlFunctions.py starting with "control_" can be called via RPC.

Call: python controlRpcServer.py
or:   pypy controlRpcServer.py

RPC-type:
- JSON_STREAM -> controlJsonRpcStreamServer (JSON-RPC stream)
- JSON -> controlJsonRpcServer (JSON-RPC)
- XML -> controlXmlRpcServer (XML-RPC)

This script works with Python 2.7 and Python 3.4.
Supports pypy (http://pypy.org/) with JIT compliation

"""

from __future__ import division, unicode_literals, print_function, absolute_import, with_statement  # Ensure compatibility with Python 3
import argparse
import sys
import os

PY3 = sys.version_info[0] == 3
PY2 = sys.version_info[0] == 2

__author__ = 'Roland Kurmann'
__email__ = 'roland dot kurmann at velasolaris dot com'
__url__ = 'http://velasolaris.com'
__license__ = 'MIT'
__version__ = '9.2'

parser = argparse.ArgumentParser(description='Polysun control function server starter.')
parser.add_argument('-t', '--rpcType', default='JSON_STREAM', help='RPC-type of server to start: JSON_SREAM, JSON, XML. Default JSON_STREAM')
parser.add_argument('-p', '--port', default='2102', type=int, help='Port of the JSON-RPC stream server (TCP socket). Default 2102')
parser.add_argument('-s', '--host', default='127.0.0.1', help='Host address of the stream server. Default 127.0.0.1')
parser.add_argument('-f', '--functions', default='controlFunctions', help='Python module with control functions. Default controlFunctions')
parser.add_argument('-m', '--modulepath', default='', help='Path for controlFunctions module. Default empty')
parser.add_argument('-d', '--debug', action='store_true', help='Enable debug mode with debug output')
parser.add_argument('-n', '--noreload', action='store_true', help='Do not automatically reload server after script changes')
parser.add_argument('-u', '--urlpath', default='/', help='Path of the JSON-RPC-Server (namespace), e.g /control. Default /')

args = parser.parse_args()

print("RPC server starter")
print("Python: " + sys.executable + " [" + str(sys.version_info[0]) + "." + str(sys.version_info[1])+ "]")

def run(filename):
    if (args.debug):
        print("RPC server script: " + filename)
    if PY2:
        execfile(filename)
    else:
        exec(open(filename).read())

path = os.path.dirname(os.path.realpath(__file__)) + os.sep
if (args.rpcType == 'JSON_STREAM'):
    import json
    import threading
    import request_jsonrpc

    run(path + 'controlJsonRpcStreamServer.py')
elif (args.rpcType == 'JSON'):
    run(path + 'controlJsonRpcServer.py')
elif (args.rpcType == 'XML'):
    run(path + 'controlXmlRpcServer.py')
else:
    print("Unknown rpcType: " + args.rpcType)
