"""
XML-RPC client listing all the available functions of an XML-RPC server

Call: python listFunctionsXmlRpcClient.py

This script works with Python 2 and Python 3
"""

from __future__ import division, unicode_literals, print_function, absolute_import, with_statement  # Ensure compatibility with Python 3
import argparse, sys
from utils import indent

__author__ = 'Roland Kurmann'
__email__ = 'roland dot kurmann at velasolaris dot com'
__url__ = 'http://velasolaris.com'
__license__ = 'MIT'
__version__ = '9.1'

PY3 = sys.version_info[0] == 3
PY2 = sys.version_info[0] == 2

if PY3:
    from xmlrpc.client import ServerProxy
elif PY2:
    from xmlrpclib import ServerProxy

parser = argparse.ArgumentParser(description='List functions of an XML-RPC-Server.')
parser.add_argument('-p', '--port', default='2101', type=int, help='Port of the XML-RPC-Server. Default 2101')
parser.add_argument('--path', default='/control', help='Path of the XML-RPC-Server (namespace). Default /control')
parser.add_argument('-d', '--debug', action='store_true', help='Enable debug mode with debug output')

args = parser.parse_args()
if args.debug:
    print("Port = " + str(args.port))
    print("Path = " + args.path)

url = "http://localhost:" + str(args.port) + args.path
print("List functions of " + url)

proxy = ServerProxy(url)

print("State: " + ("OK" if proxy.ping() == "pong" else "NOK"))

print()
functions = proxy.system.listMethods()
for function in functions:
    print(function + "():")
    # print(indent + proxy.system.methodSignature(function))
    print(proxy.system.methodHelp(function))
    print("\n")

