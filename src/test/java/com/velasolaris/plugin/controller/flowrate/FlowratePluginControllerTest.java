package com.velasolaris.plugin.controller.flowrate;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.velasolaris.plugin.controller.spi.AbstractPluginController;
import com.velasolaris.plugin.controller.spi.IPluginController;
import com.velasolaris.plugin.controller.spi.PluginControllerConfiguration;
import com.velasolaris.plugin.controller.spi.PluginControllerConfiguration.AbstractProperty.Type;
import com.velasolaris.plugin.controller.spi.PluginControllerConfiguration.ControlSignal;
import com.velasolaris.plugin.controller.spi.PluginControllerConfiguration.Log;
import com.velasolaris.plugin.controller.spi.PluginControllerConfiguration.Property;
import com.velasolaris.plugin.controller.spi.PluginControllerConfiguration.Sensor;
import com.velasolaris.plugin.controller.spi.PluginControllerException;
import com.velasolaris.plugin.controller.spi.PolysunSettings;
import com.velasolaris.plugin.controller.spi.PolysunSettings.PropertyValue;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

/**
 * JUnit tests for the FlowratePluginController.
 *
 * @author rkurmann
 * @since Polysun 9.1
 *
 */
public class FlowratePluginControllerTest {

    private static double PRECISION = 0.000001;

    private IPluginController controller;

    /**
     * Returns a PolysunSetting object as it would be returned by Polysun. This
     * object must correspond to the configuration by
     * {@link DemoFlowratePluginController#getConfiguration(Map)}.
     */
    private PolysunSettings createPolysunSettings(int flowrateType, float fixedFlowrate,
            float scaleFlowrate, Boolean measuredFlowrate, boolean measuredFlowrate2, boolean variableDesignFlowrate,
            Boolean statusPump1, Boolean status, Boolean flowratePump1) {
        List<PropertyValue> properties = new ArrayList<>();
        properties.add(new PropertyValue("Flowrate type", flowrateType, ""));
        properties.add(new PropertyValue("Fixed flowrate", fixedFlowrate, "l/h"));
        properties.add(new PropertyValue("Flowrate scale", scaleFlowrate, "l/h"));

        List<Sensor> sensors = new ArrayList<>();
        sensors.add(new Sensor("Flowrate sensor 1", "l/h", true, true, measuredFlowrate));
        sensors.add(new Sensor("Flowrate sensor 2", "l/h", true, false, measuredFlowrate2));
        sensors.add(new Sensor("Variable flowrate sensor", "l/h", true, false, variableDesignFlowrate));

        List<ControlSignal> controlSignals = new ArrayList<>();
        controlSignals.add(new ControlSignal("Signal pump 1", "", false, true, statusPump1));
        controlSignals.add(new ControlSignal("Signal pump 2", "", false, false, status));
        controlSignals.add(new ControlSignal("Flowrate pump 1", "l/h", true, false, flowratePump1));

        List<Log> logs = new ArrayList<>();

        return new PolysunSettings(properties, sensors, controlSignals, logs);
    }

    private PolysunSettings createPolysunSettingsDefaultConfiguration() {
        return createPolysunSettings(1, 10, 1, true, false, false, true, false, true);
    }

    /**
     * Creates PolysunSettingsObject from configuration.
     */
    private PolysunSettings createPolysunSettingsFromConfiguration() throws PluginControllerException {
        PluginControllerConfiguration configuration = controller.getConfiguration(null);
        List<PropertyValue> properties = new ArrayList<>();
        for (Property property : configuration.getProperties()) {
            PropertyValue propertyValue = null;
            if (property.getType() == Type.FLOAT) {
                propertyValue = new PropertyValue(property.getName(), property.getDefaultFloat(),
                        property.getUnit());
            } else if (property.getType() == Type.INTEGER) {
                propertyValue = new PropertyValue(property.getName(), property.getDefaultInt(),
                        property.getUnit());
            } else if (property.getType() == Type.STRING) {
                propertyValue = new PropertyValue(property.getName(), property.getDefaultString());
            }
            properties.add(propertyValue);
        }
        List<Sensor> sensors = new ArrayList<>();
        for (Sensor sensor : configuration.getSensors()) {
            sensors.add(new Sensor(sensor.getName(), sensor.getUnit(), sensor.isAnalog(), sensor.isRequired(),
                        "Flowrate sensor 1".equals(sensor.getName())));
        }
        List<ControlSignal> controlSignals = new ArrayList<>();
        for (ControlSignal controlSignal : configuration.getControlSignals()) {
            controlSignals.add(new ControlSignal(controlSignal.getName(), controlSignal.getUnit(),
                        controlSignal.isAnalog(), controlSignal.isRequired(),
                        "Signal pump 1".equals(controlSignal.getName()) || "Flowrate pump 1".equals(controlSignal.getName())));
        }

        List<Log> logs = new ArrayList<>();
        for (Log log : configuration.getLogs()) {
            logs.add(new Log(log.getName(), log.getUnit()));
        }

        return new PolysunSettings(properties, sensors, controlSignals, logs);
    }

    @BeforeEach
    public void setUp() throws Exception {
        controller = new FlowratePluginController();
    }

    @Test
    public void testControlDefaultSignalOff() throws PluginControllerException {
        controller.build(createPolysunSettingsDefaultConfiguration(), null);
        float[] sensors = new float[] { 5, Float.NaN, Float.NaN };
        float[] controlSignals = new float[3];
        float[] logValues = new float[3];
        int[] timepoints = controller.control(3630, true, sensors, controlSignals, logValues, false, null);
        assertNull(timepoints, "Wrong number of timepoints");
        assertEquals(0.0, controlSignals[0], PRECISION, "Wrong control signal 1");
        assertEquals(0.0, controlSignals[1], PRECISION, "Wrong control signal 2");
        assertEquals(5.0, controlSignals[2], PRECISION, "Wrong control signal 3");
    }

    @Test
    public void testControlDefaultSignalOn() throws PluginControllerException {
        controller.build(createPolysunSettingsDefaultConfiguration(), null);
        float[] sensors = new float[] { 20, Float.NaN, Float.NaN };
        float[] controlSignals = new float[3];
        float[] logValues = new float[3];
        int[] timepoints = controller.control(3630, true, sensors, controlSignals, logValues, false, null);
        assertEquals(null, timepoints, "Wrong number of timepoints");
        assertEquals(1, controlSignals[0], PRECISION, "Wrong control signal 1");
        assertEquals(1, controlSignals[1], PRECISION, "Wrong control signal 2");
        assertEquals(20, controlSignals[2], PRECISION, "Wrong control signal 3");
    }

    @Test
    public void testControlDefaultStatusOff() throws PluginControllerException {
        controller.build(createPolysunSettingsDefaultConfiguration(), null);
        float[] sensors = new float[] { 20, Float.NaN, Float.NaN };
        float[] controlSignals = new float[3];
        float[] logValues = new float[3];
        int[] timepoints = controller.control(3630, false, sensors, controlSignals, logValues, false, null);
        assertEquals(null, timepoints, "Wrong number of timepoints");
        assertEquals(0, controlSignals[0], PRECISION, "Wrong control signal 1");
        assertEquals(0, controlSignals[1], PRECISION, "Wrong control signal 2");
        assertEquals(20, controlSignals[2], PRECISION, "Wrong control signal 3");
    }

    @Test
    public void testControlFixFlowrate2SensorsSignalOff() throws PluginControllerException {
        float sensorFlowrate = 2;
        float sensorFlowrate2 = 5;
        float fixDesignFlowrate = 10;
        boolean secondFlowrate = true;
        controller.build(createPolysunSettings(1, fixDesignFlowrate, 1, true, secondFlowrate, false, true, false, true),
                null);
        float[] sensors = new float[] { sensorFlowrate, sensorFlowrate2, Float.NaN };
        float[] controlSignals = new float[3];
        float[] logValues = new float[3];

        int[] timepoints = controller.control(3630, true, sensors, controlSignals, logValues, false, null);
        assertEquals(null, timepoints, "Wrong number of timepoints");
        assertEquals(0, controlSignals[0], PRECISION, "Wrong control signal 1");
        assertEquals(0, controlSignals[1], PRECISION, "Wrong control signal 2");
        assertEquals(sensorFlowrate + sensorFlowrate2, controlSignals[2], PRECISION, "Wrong control signal 3");
    }

    @Test
    public void testControlFixFlowrate2SensorsSignalOn() throws PluginControllerException {
        float sensorFlowrate = 2;
        float sensorFlowrate2 = 15;
        float fixDesignFlowrate = 10;
        boolean secondFlowrate = true;
        controller.build(createPolysunSettings(1, fixDesignFlowrate, 1, true, secondFlowrate, false, true, false, true),
                null);
        float[] sensors = new float[] { sensorFlowrate, sensorFlowrate2, Float.NaN };
        float[] controlSignals = new float[3];
        float[] logValues = new float[3];

        int[] timepoints = controller.control(3630, true, sensors, controlSignals, logValues, false, null);
        assertEquals(null, timepoints, "Wrong number of timepoints");
        assertEquals(1, controlSignals[0], PRECISION, "Wrong control signal 1");
        assertEquals(1, controlSignals[1], PRECISION, "Wrong control signal 2");
        assertEquals(sensorFlowrate + sensorFlowrate2, controlSignals[2], PRECISION, "Wrong control signal 3");
    }

    @Test
    public void testControlFixFlowrateSignalOff() throws PluginControllerException {
        float sensorFlowrate = 0;
        float fixDesignFlowrate = 10;
        controller.build(createPolysunSettings(1, fixDesignFlowrate, 1, true, false, false, true, false, true), null);
        float[] sensors = new float[] { sensorFlowrate, Float.NaN, Float.NaN };
        float[] controlSignals = new float[3];
        float[] logValues = new float[3];

        int[] timepoints = controller.control(3630, true, sensors, controlSignals, logValues, false, null);
        assertEquals(null, timepoints, "Wrong number of timepoints");
        assertEquals(0, controlSignals[0], PRECISION, "Wrong control signal 1");
        assertEquals(0, controlSignals[1], PRECISION, "Wrong control signal 2");
        assertEquals(sensorFlowrate, controlSignals[2], PRECISION, "Wrong control signal 3");
    }

    @Test
    public void testControlFixFlowrateSignalOn() throws PluginControllerException {
        float sensorFlowrate = 50;
        float fixDesignFlowrate = 10;
        controller.build(createPolysunSettings(1, fixDesignFlowrate, 1, true, false, false, true, false, true), null);
        float[] sensors = new float[] { sensorFlowrate, Float.NaN, Float.NaN };
        float[] controlSignals = new float[3];
        float[] logValues = new float[3];

        int[] timepoints = controller.control(3630, true, sensors, controlSignals, logValues, false, null);
        assertEquals(null, timepoints, "Wrong number of timepoints");
        assertEquals(1, controlSignals[0], PRECISION, "Wrong control signal 1");
        assertEquals(1, controlSignals[1], PRECISION, "Wrong control signal 2");
        assertEquals(sensorFlowrate, controlSignals[2], PRECISION, "Wrong control signal 3");
    }

    @Test
    public void testControlFromConfigurationSignalOff() throws PluginControllerException {
        controller.build(createPolysunSettingsFromConfiguration(), null);
        float[] sensors = new float[] { 5, Float.NaN, Float.NaN };
        float[] controlSignals = new float[3];
        float[] logValues = new float[3];
        int[] timepoints = controller.control(3630, true, sensors, controlSignals, logValues, false, null);
        assertEquals(null, timepoints, "Wrong number of timepoints");
        assertEquals(0, controlSignals[0], PRECISION, "Wrong control signal 1");
        assertEquals(0, controlSignals[1], PRECISION, "Wrong control signal 2");
        assertEquals(5, controlSignals[2], PRECISION, "Wrong control signal 3");
    }

    @Test
    public void testControlFromConfigurationSignalOn() throws PluginControllerException {
        controller.build(createPolysunSettingsFromConfiguration(), null);
        float[] sensors = new float[] { 20, Float.NaN, Float.NaN };
        float[] controlSignals = new float[3];
        float[] logValues = new float[3];
        int[] timepoints = controller.control(3630, true, sensors, controlSignals, logValues, false, null);
        assertEquals(null, timepoints, "Wrong number of timepoints");
        assertEquals(1, controlSignals[0], PRECISION, "Wrong control signal 1");
        assertEquals(1, controlSignals[1], PRECISION, "Wrong control signal 2");
        assertEquals(20, controlSignals[2], PRECISION, "Wrong control signal 3");
    }

    @Test
    public void testControlVariableFlowrateSensorNotUsedSignalOff() throws PluginControllerException {
        float sensorFlowrate = 150;
        float variableDesignFlowrate = 100;
        boolean variableFlowrateSet = false;
        controller.build(createPolysunSettings(0, 10, 1, true, false, variableFlowrateSet, true, false, true), null);
        float[] sensors = new float[] { sensorFlowrate, Float.NaN, variableDesignFlowrate };
        float[] controlSignals = new float[3];
        float[] logValues = new float[3];

        int[] timepoints = controller.control(3630, true, sensors, controlSignals, logValues, false, null);
        assertEquals(null, timepoints, "Wrong number of timepoints");
        assertEquals(0, controlSignals[0], PRECISION, "Wrong control signal 1");
        assertEquals(0, controlSignals[1], PRECISION, "Wrong control signal 2");
        assertEquals(sensorFlowrate, controlSignals[2], PRECISION, "Wrong control signal 3");
    }

    @Test
    public void testControlVariableFlowrateSignalOff() throws PluginControllerException {
        float sensorFlowrate = 50;
        float variableDesignFlowrate = 100;
        boolean variableFlowrateSet = true;
        controller.build(createPolysunSettings(0, 10, 1, true, false, variableFlowrateSet, true, false, true), null);
        float[] sensors = new float[] { sensorFlowrate, Float.NaN, variableDesignFlowrate };
        float[] controlSignals = new float[3];
        float[] logValues = new float[3];

        int[] timepoints = controller.control(3630, true, sensors, controlSignals, logValues, false, null);
        assertEquals(null, timepoints, "Wrong number of timepoints");
        assertEquals(0, controlSignals[0], PRECISION, "Wrong control signal 1");
        assertEquals(0, controlSignals[1], PRECISION, "Wrong control signal 2");
        assertEquals(sensorFlowrate, controlSignals[2], PRECISION, "Wrong control signal 3");
    }

    @Test
    public void testControlVariableFlowrateSignalOn() throws PluginControllerException {
        float sensorFlowrate = 150;
        float variableDesignFlowrate = 100;
        boolean variableFlowrateSet = true;
        controller.build(createPolysunSettings(0, 10, 1, true, false, variableFlowrateSet, true, false, true), null);
        float[] sensors = new float[] { sensorFlowrate, Float.NaN, variableDesignFlowrate };
        float[] controlSignals = new float[3];
        float[] logValues = new float[3];

        int[] timepoints = controller.control(3630, true, sensors, controlSignals, logValues, false, null);
        assertEquals(null, timepoints, "Wrong number of timepoints");
        assertEquals(1, controlSignals[0], PRECISION, "Wrong control signal 1");
        assertEquals(1, controlSignals[1], PRECISION, "Wrong control signal 2");
        assertEquals(sensorFlowrate, controlSignals[2], PRECISION, "Wrong control signal 3");
    }

    @Test
    public void testControlVariableFlowrateTwoSensorsSignalOff() throws PluginControllerException {
        float sensorFlowrate = 50;
        float variableDesignFlowrate = 100;
        boolean variableFlowrateSet = true;
        float sensorFlowrate2 = 15;
        boolean secondFlowrate = true;
        controller.build(createPolysunSettings(0, 10, 1, true, secondFlowrate, variableFlowrateSet, true, false, true),
                null);
        float[] sensors = new float[] { sensorFlowrate, sensorFlowrate2, variableDesignFlowrate };
        float[] controlSignals = new float[3];
        float[] logValues = new float[3];

        int[] timepoints = controller.control(3630, true, sensors, controlSignals, logValues, false, null);
        assertEquals(null, timepoints, "Wrong number of timepoints");
        assertEquals(0, controlSignals[0], PRECISION, "Wrong control signal 1");
        assertEquals(0, controlSignals[1], PRECISION, "Wrong control signal 2");
        assertEquals(sensorFlowrate + sensorFlowrate2, controlSignals[2], PRECISION, "Wrong control signal 3");
    }

    @Test
    public void testControlVariableFlowrateTwoSensorsSignalOn() throws PluginControllerException {
        float sensorFlowrate = 50;
        float variableDesignFlowrate = 100;
        boolean variableFlowrateSet = true;
        float sensorFlowrate2 = 60;
        boolean secondFlowrate = true;
        controller.build(createPolysunSettings(0, 10, 1, true, secondFlowrate, variableFlowrateSet, true, false, true),
                null);
        float[] sensors = new float[] { sensorFlowrate, sensorFlowrate2, variableDesignFlowrate };
        float[] controlSignals = new float[3];
        float[] logValues = new float[3];

        int[] timepoints = controller.control(3630, true, sensors, controlSignals, logValues, false, null);
        assertEquals(null, timepoints, "Wrong number of timepoints");
        assertEquals(1, controlSignals[0], PRECISION, "Wrong control signal 1");
        assertEquals(1, controlSignals[1], PRECISION, "Wrong control signal 2");
        assertEquals(sensorFlowrate + sensorFlowrate2, controlSignals[2], PRECISION, "Wrong control signal 3");
    }

    @Test
    public void testControlVariableFlowrateTwoSensorsScalingSignalOn() throws PluginControllerException {
        float sensorFlowrate = 50;
        float variableDesignFlowrate = 100;
        boolean variableFlowrateSet = true;
        float sensorFlowrate2 = 60;
        boolean secondFlowrate = true;
        float scaling = 0.5f;
        controller.build(
                createPolysunSettings(0, 10, scaling, true, secondFlowrate, variableFlowrateSet, true, false, true),
                null);
        float[] sensors = new float[] { sensorFlowrate, sensorFlowrate2, variableDesignFlowrate };
        float[] controlSignals = new float[3];
        float[] logValues = new float[3];

        int[] timepoints = controller.control(3630, true, sensors, controlSignals, logValues, false, null);
        assertEquals(null, timepoints, "Wrong number of timepoints");
        assertEquals(1, controlSignals[0], PRECISION, "Wrong control signal 1");
        assertEquals(1, controlSignals[1], PRECISION, "Wrong control signal 2");
        assertEquals((sensorFlowrate + sensorFlowrate2) * scaling, controlSignals[2], PRECISION,
            "Wrong control signal 3");
    }

    @Test
    public void testControlVariableFlowrateTwoSensorsScalingStatusOff() throws PluginControllerException {
        float sensorFlowrate = 50;
        float variableDesignFlowrate = 100;
        boolean variableFlowrateSet = true;
        float sensorFlowrate2 = 60;
        boolean secondFlowrate = true;
        float scaling = 0.5f;
        controller.build(
                createPolysunSettings(0, 10, scaling, true, secondFlowrate, variableFlowrateSet, true, false, true),
                null);
        float[] sensors = new float[] { sensorFlowrate, sensorFlowrate2, variableDesignFlowrate };
        float[] controlSignals = new float[3];
        float[] logValues = new float[3];

        int[] timepoints = controller.control(3630, false, sensors, controlSignals, logValues, false, null);
        assertEquals(null, timepoints, "Wrong number of timepoints");
        assertEquals(0, controlSignals[0], PRECISION, "Wrong control signal 1");
        assertEquals(0, controlSignals[1], PRECISION, "Wrong control signal 2");
        assertEquals((sensorFlowrate + sensorFlowrate2) * scaling, controlSignals[2], PRECISION,
            "Wrong control signal 3");
    }

    @Test
    public void testGetConfiguration() throws PluginControllerException {
        PluginControllerConfiguration configuration = controller.getConfiguration(null);
        assertEquals(3, configuration.getProperties().size(), "Wrong number of configured properties");
        assertEquals(0, configuration.getNumGenericProperties(), "Wrong number of generic properties");
        assertEquals(3, configuration.getSensors().size(), "Wrong number of configured sensors");
        assertEquals(0, configuration.getNumGenericSensors(), "Wrong number of generic sensors");
        assertEquals(3, configuration.getControlSignals().size(), "Wrong number of configured controlSignals");
        assertEquals(0, configuration.getNumGenericControlSignals(), "Wrong number of generic controlSignals");
        assertEquals(0, configuration.getLogs().size(), "Wrong number of logs");
        assertEquals(configuration.getImagePath(), "plugin/images/controller_plugin.png", "Wrong controller image");

        assertEquals(createPolysunSettingsDefaultConfiguration().getSensors(),
                createPolysunSettingsFromConfiguration().getSensors(), "PolysunSetting sensors different");
        assertEquals(createPolysunSettingsDefaultConfiguration().getControlSignals(),
                createPolysunSettingsFromConfiguration().getControlSignals(), "PolysunSetting control signals different");
        assertEquals(createPolysunSettingsDefaultConfiguration().getPropertyValues(),
                createPolysunSettingsFromConfiguration().getPropertyValues(), "PolysunSetting property values different");
        assertEquals(createPolysunSettingsDefaultConfiguration(),
                createPolysunSettingsFromConfiguration(), "PolysunSetting configuration different");
    }

    @Test
    public void testGetControlSignalsToHide() {
        int state = 1;
        List<String> controlSignalsToHide = controller.getControlSignalsToHide(
                createPolysunSettings(state, 10, 1, true, false, false, true, false, true), null);
        assertEquals(null, controlSignalsToHide, "No signals to hide expected");
        state = 0;
        controlSignalsToHide = controller.getControlSignalsToHide(
                createPolysunSettings(state, 10, 1, true, false, false, true, false, true), null);
        assertEquals(null, controlSignalsToHide, "No signals to hide expected");
    }

    @Test
    public void testGetName() {
        assertEquals(controller.getName(), "Flowrate");
    }

    @Test
    public void testGetPropertiesToHide() {
        int state = 1;
        List<String> propertiesToHide = controller
            .getPropertiesToHide(createPolysunSettings(state, 10, 1, true, false, false, true, false, true), null);
        assertEquals(0, propertiesToHide.size(), "Wrong number of propreties to hide");
        state = 0;
        propertiesToHide = controller
            .getPropertiesToHide(createPolysunSettings(state, 10, 1, true, false, false, true, false, true), null);
        assertEquals(1, propertiesToHide.size(), "Wrong number of propreties to hide");
        assertEquals(propertiesToHide.get(0), "Fixed flowrate");
    }

    @Test
    public void testGetSensorsToHide() {
        int state = 1;
        List<String> sensorsToHide = controller
            .getSensorsToHide(createPolysunSettings(state, 10, 1, true, false, false, true, false, true), null);
        assertEquals(1, sensorsToHide.size(), "Wrong number of sensors to hide");
        assertEquals(sensorsToHide.get(0), "Variable flowrate sensor");
        state = 0;
        sensorsToHide = controller
            .getSensorsToHide(createPolysunSettings(state, 10, 1, true, false, false, true, false, true), null);
        assertEquals(0, sensorsToHide.size(), "No sensors to hide expected");
    }

    @Test
    public void testPolysunSettingsGetter() throws PluginControllerException {
        controller.build(createPolysunSettingsDefaultConfiguration(), null);
        assertEquals("Flowrate type", ((AbstractPluginController) controller).getProperty("Flowrate type").getName(),
            "Wrong property name returned");
        assertEquals("Flowrate sensor 1",
            ((AbstractPluginController) controller).getSensor("Flowrate sensor 1").getName(),
            "Wrong sensor name returned");
        assertEquals("Signal pump 1",
            ((AbstractPluginController) controller).getControlSignal("Signal pump 1").getName(),
            "Wrong control signal name returned");

        assertEquals(null, ((AbstractPluginController) controller).getProperty("XXX"), "Inexistent property failed");
        assertEquals(null, ((AbstractPluginController) controller).getSensor("XXX"), "Inexistent property failed");
        assertEquals(null, ((AbstractPluginController) controller).getControlSignal("XXX"),
            "Inexistent property failed");

        assertEquals(0, ((AbstractPluginController) controller).getSensorIndex("Flowrate sensor 1"),
            "Wrong sensor index returned");
        assertEquals(2, ((AbstractPluginController) controller).getSensorIndex("Variable flowrate sensor"),
            "Wrong sensor index returned");
        assertEquals(0, ((AbstractPluginController) controller).getControlSignalIndex("Signal pump 1"),
            "Wrong control signal index returned");
        assertEquals(2, ((AbstractPluginController) controller).getControlSignalIndex("Flowrate pump 1"),
            "Wrong control signal index returned");

        assertEquals(-1, ((AbstractPluginController) controller).getSensorIndex("XXX"), "Wrong sensor index returned");
        assertEquals(-1, ((AbstractPluginController) controller).getControlSignalIndex("XXX"),
            "Wrong control signal index returned");
    }


}
