package com.velasolaris.plugin.controller.python;

import com.velasolaris.plugin.controller.rpc.SimpleRpcPluginController;
import com.velasolaris.plugin.controller.spi.PluginControllerConfiguration.ControlSignal;
import com.velasolaris.plugin.controller.spi.PluginControllerConfiguration.Log;
import com.velasolaris.plugin.controller.spi.PluginControllerConfiguration.Sensor;
import com.velasolaris.plugin.controller.spi.PluginControllerException;
import com.velasolaris.plugin.controller.spi.PolysunSettings;
import com.velasolaris.plugin.controller.spi.PolysunSettings.PropertyValue;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;

/**
 * Simple RPC test to test the basic python communication.
 */
public class PythonRpcControllerTest {
    Process pythonProcess;

    @BeforeEach
    void setup() throws Exception {
        String pythonScriptPath = "src/test/python/controlXmlRpcServer.py";
        pythonProcess = shellExec("python3 " + pythonScriptPath);
        Thread.sleep(1000);
    }

    @AfterEach
    void teardown() {
        pythonProcess.destroyForcibly();
    }

    @Test
    void pythonRpcControllerCommunicationTest() throws PluginControllerException {
        List<PropertyValue> propertyValues = List.of(
            new PropertyValue("RPC function", "control_flowrate"),
            new PropertyValue("RPC server URL", "http://127.0.0.1:2102/"),
            new PropertyValue("RPC type", 2, null), // Type 2 = "XML-RPC"
            new PropertyValue("Verbose level", "Standard"),
            new PropertyValue("Fixed timestep", 60, "Second"),
            new PropertyValue("Number of logs", 0, null),
            new PropertyValue("Number of sensors", 3, null),
            new PropertyValue("Number of controls signals", 3, null)
        );

        List<Sensor> sensors = List.of(
            new Sensor("Flowrate sensor 1", "l/h", true, true, true),
            new Sensor("Flowrate sensor 2", "l/h", true, false, true),
            new Sensor("Variable flowrate sensor", "l/h", true, false, true)
        );

        List<ControlSignal> controlSignals = List.of(
            new ControlSignal("Signal pump 1", "", false, true, true),
            new ControlSignal("Signal pump 2", "", false, false, true),
            new ControlSignal("Flowrate pump 1", "l/h", true, false, true)
        );

        List<Log> logs = List.of();


        SimpleRpcPluginController controller = new SimpleRpcPluginController();
        PolysunSettings settings = new PolysunSettings(propertyValues, sensors, controlSignals, logs);
        controller.build(settings, new HashMap<>());

        float[] sensorValues = new float[]{-1, 1, 0};
        float[] controlSignalValues = new float[]{1, 2, 3};
        float[] logValues = new float[]{};

        int[] results = controller.control(1, true,
            sensorValues, controlSignalValues, logValues, false, Map.of());

        // our mock controller script will return "1" for input sensor value that is >= 1.0
        assertArrayEquals(new int[]{0, 1, 0}, results);
    }

    Process shellExec(String command) throws IOException {
        Runtime rt = Runtime.getRuntime();
        return rt.exec(command);
    }

}
