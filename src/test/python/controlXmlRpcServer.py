#!/usr/bin/env python
"""
Simplified version of the XML-RPC Server for Polysun plugin controller functions
of SimpleRpcPluginController (RPC-Type: XML-RPC). This version is slightly simplified
just for the use in unit tests
"""

from __future__ import division, unicode_literals, print_function, absolute_import, with_statement  # Ensure compatibility with Python 3
import argparse
import inspect
import sys
import importlib

PY3 = sys.version_info[0] == 3
PY2 = sys.version_info[0] == 2

if PY3:
    from xmlrpc.server import SimpleXMLRPCServer
    from xmlrpc.server import SimpleXMLRPCRequestHandler
elif PY2:
    from SimpleXMLRPCServer import SimpleXMLRPCServer
    from SimpleXMLRPCServer import SimpleXMLRPCRequestHandler

__author__ = 'Roland Kurmann'
__email__ = 'roland dot kurmann at velasolaris dot com'
__url__ = 'http://velasolaris.com'
__license__ = 'MIT'
__version__ = '9.2'

parser = argparse.ArgumentParser(description='Python Polysun control function (XML-RPC HTTP Server).')
parser.add_argument('-p', '--port', default='2102', type=int, help='Port of the XML-RPC-Server. Default 2102')
parser.add_argument('-s', '--host', default='127.0.0.1', help='Host address of the REST-Server. Default 127.0.0.1')
parser.add_argument('-u', '--urlpath', default='/', help='Path of the XML-RPC-Server (namespace), e.g /control. Default /')
parser.add_argument('-f', '--functions', default='controlFunctions', help='Python module with control functions. Default controlFunctions')
parser.add_argument('-m', '--modulepath', default='', help='Path for controlFunctions module. Default empty')
parser.add_argument('-d', '--debug', action='store_true', help='Enable debug mode with debug output')
parser.add_argument('-t', '--rpcType', default='JSON_STREAM', help='Ignored in this script')

args = parser.parse_args()

if (args.modulepath != ''):
    sys.path.append(args.modulepath)
    print("Append module path " + args.modulepath)

# instead of 'import controlFunctions', load dynamically using arguments
importlib.import_module(args.functions)

# Restrict to a particular path.
class RequestHandler(SimpleXMLRPCRequestHandler):
    if (args.urlpath != ''):
        rpc_paths = (args.urlpath,)

print("Start XML-RPC server...")
print("http://" + args.host + ":" + str(args.port) + args.urlpath)

# Create server
server = SimpleXMLRPCServer((args.host, args.port),
                            requestHandler=RequestHandler,
                            logRequests=args.debug,
                            allow_none=None)
server.register_introspection_functions()
# server.register_multicall_functions()

def ping():
    """Returns "pong"."""
    return "pong"

server.register_function(ping)

def print_msg(msg):
    """Prints a message on the server."""
    print(msg)
    return []

server.register_function(print_msg, "print")

# http://stackoverflow.com/questions/4040620/is-it-possible-to-list-all-functions-in-a-module
functions = inspect.getmembers(sys.modules[args.functions], inspect.isfunction)
for function in functions:
    if function[0].startswith("control_"):
        server.register_function(function[1])

server.register_function(quit, 'stop')
# server.register_instance(MetaService())

print("Ctrl-C to stop")

try:
    # Run the server's main loop
    server.serve_forever()
except KeyboardInterrupt:
    print("\nKeyboard interrupt received, exiting.")
    server.server_close()
    sys.exit(0)
