from __future__ import division, unicode_literals, print_function, absolute_import, with_statement  # Ensure compatibility with Python 3

__author__ = 'Roland Kurmann'
__email__ = 'roland dot kurmann at velasolaris dot com'
__url__ = 'http://velasolaris.com'
__license__ = 'MIT'
__version__ = '9.1'

def control_flowrate(simulationTime, status, sensors, sensorsUsed, properties, propertiesStr, preRun, controlSignalsUsed, numLogValues, stage, fixedTimestep, verboseLevel, parameters):
    controlSignals = [0] * len(sensors)
    logValues = [0] * numLogValues
    timepoints = [1 if sensorVal >= 1.0 else 0 for sensorVal in sensors ]


    return controlSignals, logValues, timepoints
