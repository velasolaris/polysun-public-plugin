# polysun-public-plugin

Public Polysun Plugin
=====================

This is an open source Polysun plugin with controllers for Polysun simulations.

The following controllers are contained in this plugin:

- RPC Stream Plugin Controller (with Python libraries)
- Matlab Plugin Controller (Matlab)
- Flowrate Plugin Controller (Java)

Controller plugins are automatically detected by Polysun, see the ControllerPlugin interface.
This code is Open Source, see [LICENSE.txt](https://bitbucket.org/velasolaris/polysun-public-plugin/src/master/LICENSE.txt). It can be used as a reference for the development of new Polysun plugins.
If you would like to request your plugin to be included in Polysun, create a [fork of this project](https://confluence.atlassian.com/bitbucket/forking-a-repository-221449527.html) (Bitbucket account required) and create a [pull request](https://www.atlassian.com/git/tutorials/making-a-pull-request).
The plugins in this project are built into Polysun. To develop external plugins, please create a [fork](https://confluence.atlassian.com/bitbucket/forking-a-repository-221449527.html) of the [polysun-demo-plugin](https://bitbucket.org/velasolaris/polysun-demo-plugin/) project or create your own project from scratch.

Requirements for controller plugin development
----------------------------------------------

- Liberica Open[JDK 17](https://bell-sw.com/pages/downloads/#/java-17-current)
- [Gradle Build Tool](https://gradle.org/)

Build JAR
---------

Linux/Mac: ./`gradlew fatJar`
Windows: `gradlew.bat fatJar`

Development
-----------

To import the project into Eclipse, use File > Import... > Gradle > Existing Gradle Project.

Please refer to the following API documentations:

- [This project's Javadoc](https://javadoc.io/doc/com.velasolaris.polysun/polysun-public-plugin)
- [Polysun Plugin Interface API](https://javadoc.io/doc/com.velasolaris.polysun/polysun-plugin-if)

Classes to get started with:

- IPluginController: Interface for plugin controllers
- AbstractPluginController: Abstract class for plugin controllers that implements the IPluginController interface
- ControllerPlugin: Interface for plugin detection
- AbstractControllerPlugin: Abstract class with the default implementations for detecting plugins.
